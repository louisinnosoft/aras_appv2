string strMethodName = "ApplyIn_PLM2ERP";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strMsg = "";

Item itmR = this;

try
{
	string strMethod = "In_PLM2ERP";
	aml = "<AML>";
	aml += "<Item type='Project' action='get' select='id'>";
	aml += "</Item></AML>";

	Item itmTmps = inn.applyAML(aml);
	for(int i=0;i<itmTmps.getItemCount();i++)
	{
		Item itmTmp = itmTmps.getItemByIndex(i);
		strMsg += itmTmp.getID() + ",";
		itmTmp.apply(strMethod);
	}
	strMsg = itmTmps.getItemCount().ToString() + "筆:" + strMsg;
	itmR = inn.newResult(strMsg);
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ApplyIn_PLM2ERP' and [Method].is_current='1'">
<config_id>92753116FD3447C5A63B529FD55E9EC8</config_id>
<name>ApplyIn_PLM2ERP</name>
<comments>ApplyIn_PLM2ERP</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
