string FileType = this.getProperty("id");
if (!String.IsNullOrEmpty(FileType)) 
  FileType = FileType.Replace("'","''");
  
string FileExtension = this.getProperty("extension");
if (!String.IsNullOrEmpty(FileExtension)) 
  FileExtension = FileExtension.Replace("'","''");
  
string sql = String.Format(CultureInfo.InvariantCulture, "UPDATE [FILE] SET file_type = '{0}' WHERE filename like '%.{1}' AND file_type IS NULL", FileType, FileExtension);
CCO.DB.InnDatabase.ExecuteSQL(sql);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Associate files' and [Method].is_current='1'">
<config_id>D9FD61B9A9B3461098E0AF75B8848F29</config_id>
<name>Associate files</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
