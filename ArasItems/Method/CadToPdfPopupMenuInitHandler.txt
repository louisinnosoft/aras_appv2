//Copied from CustomActionHelper.js
var isEnabled = aras.commonProperties.isConvertCADToPDFEnabled;
if (typeof(isEnabled) === 'undefined') {
	try {
		isEnabled = !!aras.IomInnovator.ConsumeLicense('Aras.CADConverter2');
	} catch (exception) {
		isEnabled = false;
	}
	aras.commonProperties.isConvertCADToPDFEnabled = isEnabled;
}

return {'cui_invisible': !isEnabled};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CadToPdfPopupMenuInitHandler' and [Method].is_current='1'">
<config_id>05B5615D49DD48DAB2927302CEFCD58E</config_id>
<name>CadToPdfPopupMenuInitHandler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
