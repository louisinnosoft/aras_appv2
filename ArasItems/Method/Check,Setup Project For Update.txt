'inDom format:
'  <Item type='Method' action='Check,Setup Project For Update'>
'    <project_item>
'      <Item Type='Project' id='PROJECT_ID_VAL'>
'        <state>StateValue</state>
'        <wbs_id>WbsIdValue</wbs_id>
'        <locked_by_id>LockedByIdValue</locked_by_id>
'      </Item>
'    </project_item>
'  </Item>
' OR
'  <Item type='Method' action='individualUpdate'>
'    <project_item><Item type='Project' id='PROJECT_ID_VAL'/></project_item>
'  </Item>

Dim tmpInn As Innovator = Me.getInnovator()
Dim q As Item = tmpInn.newItem("tmp", "tmp")
Dim r As Item
Dim tmpNd As XmlElement = CType(Me.node.selectSingleNode("project_item/Item"), XmlElement)
If IsNothing(tmpNd) Then Return tmpInn.newError("Invalid input parameters.")

Dim projectItem As XmlElement = tmpNd
Dim projectID As String = projectItem.getAttribute("id")
'if state and wbs_id are specified and project is unlocked then locked_by_id does not exist
tmpNd = CType(projectItem.selectSingleNode("node()[state and wbs_id]"), XmlElement)
If IsNothing(tmpNd) Then
  q.loadAML("<Item type='Project' action='get' select='state,wbs_id,locked_by_id'/>")
  q.setAttribute("id", projectID)
  r = q.apply()
  If r.isError() Then Return r
  projectItem = r.node
End If

Dim projectState As String = CCO.XML.GetItemProperty(projectItem, "state")
Dim wbsID As String = CCO.XML.GetItemProperty(projectItem, "wbs_id")
Dim lockedById As String = CCO.XML.GetItemProperty(projectItem, "locked_by_id")

If IsNothing(wbsID) OrElse wbsID = "" Then Return tmpInn.newError("Project has no root WBS Element")
If Not IsNothing(lockedById) AndAlso lockedById.Length > 0 Then Return tmpInn.newError("project is locked")
If projectState <> "Active" Then Return tmpInn.newError("Project state must be Active")

Return tmpInn.newResult("ok")
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Check,Setup Project For Update' and [Method].is_current='1'">
<config_id>A813A4CF2FC349D09636B2BE3CD1AB83</config_id>
<name>Check,Setup Project For Update</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
