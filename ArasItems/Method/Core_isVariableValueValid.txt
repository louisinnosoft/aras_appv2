var userPwdDigigtsMinNumber = 'User_pwd_digits_min_number';
var userPwdSymbolsMinNumber = 'User_pwd_symbols_min_number';
var accLockTresholdTries = 'AccountLockoutThreshold_triesNum';
var accLockDurationMinutes = 'AccountLockoutDuration_minutes';

var itm = document.item;
if (itm) {
	var propName = aras.getItemProperty(itm, 'name');
	if (propName == userPwdDigigtsMinNumber || propName == userPwdSymbolsMinNumber ||
		propName == accLockTresholdTries || propName == accLockDurationMinutes) {
		if (!aras.isInteger(this.value)) {
			if (aras.confirm(aras.getResource('', 'imports_core.variable_value_must_integer'))) {
				srcElement.focus();
				aras.setItemProperty(document.item, propName, '0');
			} else {
				srcElement.value = '0';
				if (window.handleItemChange) {
					handleItemChange(propName, '0');
				}
			}
		}
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Core_isVariableValueValid' and [Method].is_current='1'">
<config_id>ED77309DC62B41969B64DA0FECC358F3</config_id>
<name>Core_isVariableValueValid</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
