if (this.getProperty('crawler_state', '') === 'Active') {

	var crawlerState = this.newItem('ES_CrawlerState', 'get');
	crawlerState.setAttribute('select', 'current_action, next_action');
	crawlerState.setProperty('source_id', this.getID());
	crawlerState = crawlerState.apply();

	if (!crawlerState.isError() && crawlerState.getItemCount() === 1) {

		if (crawlerState.getProperty('current_action', '') === 'Pause') {
			crawlerState.setProperty('next_action', 'Resume');

			var iLockStatus = crawlerState.getLockStatus();

			if (iLockStatus === 0) {
				crawlerState.apply('edit');
			}

			if (iLockStatus === 1) {
				crawlerState.apply('update');
			}
		} else {
			aras.AlertError('\'Resume\' action can be used only after \'Pause\' action.');
		}
	} else {
		aras.AlertError(crawlerState.getErrorMessage());
	}
} else {
	aras.AlertError('A crawler should be in \'Active\' state to perform any operations.');
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_ResumeCrawler' and [Method].is_current='1'">
<config_id>E2F554B0AE334A06AB9D58FFF6054E4E</config_id>
<name>ES_ResumeCrawler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
