if (grid.getRowCount() > 0) {

	var rootItem = parent.thisItem;
	var relItem = rootItem.getItemsByXPath('//Item[@id=\'' + relationshipID + '\']').getItemByIndex(0);

	if (rootItem !== null && relItem !== null) {
		rootItem.removeRelationship(relItem);
		alert('It\'s not allowed to add more that one ' + relItem.getType() + ' row.');
	}

	return false;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_RowCanBeAddedCheck' and [Method].is_current='1'">
<config_id>07C6E9BB6DCA48AB95917F504AD27487</config_id>
<name>ES_RowCanBeAddedCheck</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
