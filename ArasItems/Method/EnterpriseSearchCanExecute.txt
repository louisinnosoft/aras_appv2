var canExecute = aras.commonProperties.isESFeatureEnabled;
if (typeof(canExecute) === 'undefined') {
	try {
		canExecute = !!aras.IomInnovator.ConsumeLicense('Aras.EnterpriseSearch');
	} catch (exception) {
		canExecute = false;
	}
	aras.commonProperties.isESFeatureEnabled = canExecute;
}

return canExecute;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='EnterpriseSearchCanExecute' and [Method].is_current='1'">
<config_id>446F2FA6A1D944E8B12C6285C704185D</config_id>
<name>EnterpriseSearchCanExecute</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
