if (!inArgs) {
	return;
}

var item = inArgs.item;
if (item.getAttribute('type') == 'FileExchangePackage') {
	var state = item.selectSingleNode('state');
	if (state !== null && state.text != 'InWork') {
		top.aras.AlertError(top.aras.getResource('', 'fe.error_add_files_to_package'));
		return;
	}
}

var params = top.aras.newObject();
params.title = top.aras.getResource('', 'fe.copy_files_from_instances_title');
params.aras = top.aras;
params.requestedPropertiesLabels = top.aras.newArray
(
	top.aras.getResource('', 'fe.selected_items'),
	top.aras.getResource('', 'fe.item_type'),
	top.aras.getResource('', 'fe.selected_files'),
	top.aras.getResource('', 'fe.changed_file_list'),
	top.aras.getResource('', 'fe.selected_all')
);
params.requestedPropertiesColumnsWidth = top.aras.newArray('20', '20', '40', '10', '10');
params.requestedPropertiesColumnsAligns = top.aras.newArray('left', 'left', 'left', 'center', 'center');

var form = top.aras.getItemByName('Form', 'FE_AddFilesToPackage', 0);
var width = top.aras.getItemProperty(form, 'width');
var height = top.aras.getItemProperty(form, 'height');
if (!width) {
	width = 800;
}
if (!height) {
	height = 600;
}
params.formId = top.aras.getItemProperty(form, 'id');
params.dialogWidth = width;
params.dialogHeight = height;
params.content = 'ShowFormAsADialog.html';

return top.ArasModules.Dialog.show('iframe', params).promise;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='FE_AddFilesToPackage' and [Method].is_current='1'">
<config_id>9CFE8429337D48E39B496F8E9F4B33C7</config_id>
<name>FE_AddFilesToPackage</name>
<comments>Add files to the package from other instances</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
