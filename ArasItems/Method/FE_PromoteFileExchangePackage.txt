Item packageFile = this.newItem("FileExchangePackageFile", "get");
packageFile.setAttribute("select", "related_id(id)");
Item query = this.newItem("FileExchangePackage", "get");
query.setID(this.getID());
query.addRelationship(packageFile);
query = query.apply();
if (query.isError())
{
	return query;	
}

Item files = query.getRelationships("FileExchangePackageFile");
if (files.getItemCount() == 0)
{
	Innovator inn = this.getInnovator();
	return inn.newError("You must add some file(s) to the package before promote");		
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='FE_PromoteFileExchangePackage' and [Method].is_current='1'">
<config_id>BA52F2FC5A8D4C348836F2820826B637</config_id>
<name>FE_PromoteFileExchangePackage</name>
<comments>Methpd for promote package to state &apos;Complete&apos;</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
