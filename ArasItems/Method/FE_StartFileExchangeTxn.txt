var aml = '<AML><Item type=\'FileExchangeTxn\' id=\'' + this.getID() + '\' /></AML>';
var query = top.aras.soapSend('StartFileExchangeTxn', aml);
if (query.isFault()) {
	top.aras.AlertError(query);
	return;
}
top.aras.AlertSuccess(top.aras.getResource('', 'fe.retry_transaction', this.getProperty('keyed_name')));

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='FE_StartFileExchangeTxn' and [Method].is_current='1'">
<config_id>73C8AC3216594E09ACE31270E765E0DF</config_id>
<name>FE_StartFileExchangeTxn</name>
<comments>Start upload/download files</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
