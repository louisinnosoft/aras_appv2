			Innovator inn = this.getInnovator();
			string cls = this.getProperty("classification", "").ToLowerInvariant();
			if (cls == "team" || cls == "system")
			{
				Item q = this.newItem();
				q.loadAML("<Item type='Member' action='get' select='related_id'><source_id>" + this.getID() + "</source_id></Item>");
				q = q.apply();
				if (q.getItemCount() >= 1)
				{
					return inn.newError("Classification could not be changed. Identity with classification=" + this.getProperty("classification", "") + " should not have Member relationships.");
				}
			}
			return null;
			
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Identity_IsMemberValid' and [Method].is_current='1'">
<config_id>AFE77197302A46C7ADA84B02A93A6331</config_id>
<name>Identity_IsMemberValid</name>
<comments>Check if Team identity should not have any member relationships.</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
