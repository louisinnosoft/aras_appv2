//目的:將物件掛入流程表單
/*做法:
Client:
1.本表單已經檢查為解鎖狀態
2.檢查本表單目前已有Workflow,則不允許執行
3.呼叫 Server 啟動in_workflowForm,並將資訊帶過去
4.將in_workflow_form 與 controlled item 更新至 workflow process 內

*/
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmWF=null;

try
{
    if(this.getProperty("state","").ToLower() == "start" || this.getProperty("state","").ToLower() == "preliminary" || this.getProperty("state","").ToLower() == "new")
    {
        Item itmWFP = this.apply("In_IsExistWorkflow");
        if (itmWFP.getProperty("is_exist", "0") == "1")
        {
            throw new Exception(itmWFP.getProperty("message", "流程已存在"));
        }
        itmWF = _InnH.AddtoWorkflow(this);
        if(itmWF!=null)
            if(itmWF.isError())
            {
                throw new Exception(itmWF.getErrorString());
            }
    }
    else
    {
        throw new Exception(_InnH.Translate("本文件狀態為:[" + this.getProperty("state","") + "],無法送審"));
    }
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = (ex.InnerException==null?ex.Message:ex.InnerException.Message);
	string strErrorDetail="";
	if(strError=="Exception of type 'Aras.Server.Core.InnovatorServerException' was thrown.")
	{
		strError = "無法執行AML:" + aml ;
	}
	strErrorDetail = strError + "\n" + ex.ToString() + aml  + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");

    throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmWF;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AddtoWorkflow_S' and [Method].is_current='1'">
<config_id>10DDA435DC284BB18C66D7468E958653</config_id>
<name>In_AddtoWorkflow_S</name>
<comments>in_core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
