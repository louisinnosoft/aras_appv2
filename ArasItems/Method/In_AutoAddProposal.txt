//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";

aml ="<AML>";
aml += "<Item type='In_Budget' action='get'>";
//aml += "<in_proposal condition='is null'></in_proposal>";
aml += "</Item></AML>";
Item itmBudgets = inn.applyAML(aml);

for(int i=0;i<itmBudgets.getItemCount();i++){
    Item itmBudget = itmBudgets.getItemByIndex(i);
    
    aml ="<AML>";
    aml += "<Item type='Project' action='get'>";
    aml += "<id>"+itmBudget.getProperty("in_refproject","")+"</id>";
    aml += "</Item></AML>";
    Item itmProject = inn.applyAML(aml);
    
    if(!itmProject.isError()){
        
        sql = "Update In_Budget set ";
        sql += "in_proposal = '" + itmProject.getProperty("in_from_item","") + "'";
        sql += " where id = '" + itmBudget.getID() + "'";
        inn.applySQL(sql);
    }
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AutoAddProposal' and [Method].is_current='1'">
<config_id>58F04407699749A49C8C6107BC77840F</config_id>
<name>In_AutoAddProposal</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
