/*
目的:計算銷貨項的稅額與含稅價(類行為銷貨單才執行)
位置:銷貨單的AfterUpdate
做法:
1.本次銷貨未稅(in_sale_price) = 銷貨單價未稅(in_ori_sales_unit_price) * 數量(in_qty)
2.累計各階銷貨未稅金額(in_sale_price)
3.滾算表頭 本次銷貨價格(in_fin_price),稅額(in_tax), 含稅銷貨價格(in_fin_price_tax)
4.計算子階稅額(in_tax) = 子階未稅價(in_sale_price) * 稅率
5.撫平稅額:累計稅額與表頭稅額比對,若有差異,則於最後一筆扣掉
6.計算子階含稅價(in_sale_price_tax) =  子階未稅價(in_sale_price) + 計算子階稅額(in_tax)
7.累計各階銷貨含稅金額(in_sale_price_tax),稅額(in_tax)
8.第一階的未稅單價(in_ori_sales_unit_price) = 未稅總價(in_sale_price) / 數量(in_qty), 但因為單價太小了,建議應取到幣別小數位數後兩位
*/
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
string aml = "";
string sql = "";
Item itmTemp;
string strLastLev3Invoice_DetailId = "";
Item itmInvoice_Detail;

//若不是部分銷貨單,則離開
if(this.getProperty("in_invoice_class","0")!="1-1" )
return this;

//稅率
decimal dblTaxRate = Convert.ToDecimal(this.getProperty("in_tax_rate","0"));

//小數位數
int intDec = Convert.ToInt32(this.getProperty("in_currency_decimal","0"));

//1.本次銷貨未稅(in_sale_price) = 銷貨單價未稅(in_ori_sales_unit_price) * 數量(in_qty)
sql = "Update [In_Invoice_Details] set in_sale_price=ROUND(CAST((in_ori_sales_unit_price*in_qty)  AS decimal(18,6))," + intDec + ") where [source_id] ='" + this.getID() + "'";
itmTemp = inn.applySQL(sql);

//2.累計各階金額
//重新作一次從下往上算,第三階累加 銷貨未稅 ,碰到第二階就累加並且歸零
aml = "<AML>";
aml += "<Item type='In_Invoice_Details' action='get' orderBy='sort_order DESC' >";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";

Item itmInvoice_Details = inn.applyAML(aml);

decimal dblSalesPrice = 0;   //銷貨未稅
decimal dblSalesPrice_1 = 0; //第1階銷貨未稅
decimal dblSalesPrice_2 = 0; //第2階銷貨未稅
decimal dblSalesPrice_3 = 0; //第3階銷貨未稅
string strLevel1Name = ""; //第一階的品名
for(int k=0;k<itmInvoice_Details.getItemCount();k++)
{
	itmInvoice_Detail = itmInvoice_Details.getItemByIndex(k);
	string strLev = itmInvoice_Detail.getProperty("in_level","0");
	dblSalesPrice = Convert.ToDecimal(itmInvoice_Detail.getProperty("in_sale_price","0")); 	
	switch(strLev)
	{
		case "1":
			dblSalesPrice_1 += dblSalesPrice_2;
			sql = "Update [In_Invoice_Details] set in_sale_price=" +dblSalesPrice_2.ToString() + " where id='" + itmInvoice_Detail.getID() + "'";
			itmTemp = inn.applySQL(sql);
			//itmInvoice_Detail.setProperty("in_sale_price",dblSalesPrice_2.ToString());
			//itmInvoice_Detail.apply("edit");
			dblSalesPrice_2 =0;
			strLevel1Name = itmInvoice_Detail.getProperty("in_part_name","");
			break;
		case "2":
			dblSalesPrice_2 += dblSalesPrice_3;
			sql = "Update [In_Invoice_Details] set in_sale_price=" +dblSalesPrice_3.ToString() + " ,in_level1_name='" + strLevel1Name + "' where id='" + itmInvoice_Detail.getID() + "'";
			itmTemp = inn.applySQL(sql);
			//itmInvoice_Detail.setProperty("in_sale_price",dblSalesPrice_3.ToString());
			//itmInvoice_Detail.apply("edit");
			dblSalesPrice_3 =0;
			
			break;
		case "3":
			dblSalesPrice_3 += dblSalesPrice;
			strLastLev3Invoice_DetailId = itmInvoice_Detail.getID();
			break;
		default:
			break;
	}
}

//3.滾算表頭 本次銷貨價格(in_fin_price),稅額(in_tax), 含稅銷貨價格(in_fin_price_tax)
decimal dblHeaderTax = Innosoft.InnUtility.Round(dblSalesPrice_1 * dblTaxRate, intDec);
decimal dblHeadFinPriceTax = dblSalesPrice_1 + dblHeaderTax;

sql = "Update [In_Invoice] set ";
sql += "in_fin_price=" + dblSalesPrice_1;
sql += ",in_tax=" + dblHeaderTax;
sql += ",in_fin_price_tax =" + dblHeadFinPriceTax;
sql += " where id='" + this.getID() + "'";
itmTemp = inn.applySQL(sql);

//4.計算子階稅額(in_tax) = 子階未稅價(in_sale_price) * 稅率
sql = "Update [In_Invoice_Details] set in_tax = ROUND(CAST((in_sale_price*" + dblTaxRate + ")  AS decimal(18,6))," + intDec + ")  where [source_id] ='" + this.getID() + "'";
itmTemp = inn.applySQL(sql);

//5.撫平稅額:累計稅額與表頭稅額比對,若有差異,則於最後一筆扣掉
sql  = "Select sum(in_tax) as c1 from [In_Invoice_Details] where [source_id] ='" + this.getID() + "' and in_level=3";
itmTemp = inn.applySQL(sql);
decimal dblSumDetailTax = Convert.ToDecimal(itmTemp.getProperty("c1","0"));
decimal dblDiffTax = dblHeaderTax - dblSumDetailTax;
if(dblDiffTax!=0)
{
	sql = "Update [In_Invoice_Details] set in_tax=in_tax+" + dblDiffTax + " where id='" + strLastLev3Invoice_DetailId + "'";
	itmTemp = inn.applySQL(sql);
}	

//6.計算子階含稅價(in_sale_price_tax) =  子階未稅價(in_sale_price) + 計算子階稅額(in_tax)
sql = "Update [In_Invoice_Details] set in_sale_price_tax = (in_sale_price+in_tax) where [source_id] ='" + this.getID() + "'";
itmTemp = inn.applySQL(sql);

//7.累計各階銷貨含稅金額(in_sale_price_tax),稅額(in_tax)
//從下往上算,第三階累加,碰到第二階就累加並且歸零
aml = "<AML>";
aml += "<Item type='in_Invoice_Details' action='get' orderBy='sort_order DESC' >";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";

itmInvoice_Details = inn.applyAML(aml);
decimal dblSalesPriceTax = 0; //含稅銷貨價
decimal dblSalesPriceTax_1 = 0; //第1階含稅銷貨價
decimal dblSalesPriceTax_2 = 0; //第2階含稅銷貨價
decimal dblSalesPriceTax_3 = 0; //第3階含稅銷貨價

decimal dblTax = 0;//稅額
decimal dblTax_1 = 0; //第1階稅額
decimal dblTax_2 = 0;//第2階稅額
decimal dblTax_3 = 0;//第3階稅額



for(int k=0;k<itmInvoice_Details.getItemCount();k++)
{
	itmInvoice_Detail = itmInvoice_Details.getItemByIndex(k);
	string strLev = itmInvoice_Detail.getProperty("in_level","0");
	dblSalesPriceTax = Convert.ToDecimal(itmInvoice_Detail.getProperty("in_sale_price_tax","0")); 
	dblTax = Convert.ToDecimal(itmInvoice_Detail.getProperty("in_tax","0")); 
	
	switch(strLev)
	{
		case "1":
			dblSalesPriceTax_1 += dblSalesPriceTax_2;
			dblTax_1 += dblTax_2;
			
			sql = "Update [In_Invoice_Details] set ";
			sql += "in_sale_price_tax=" + dblSalesPriceTax_2.ToString();
			sql += ",in_tax=" + dblTax_2.ToString();
			sql += " where id='" + itmInvoice_Detail.getID() + "'";
			itmTemp = inn.applySQL(sql);
			
//			itmQuote_Detail.setProperty("in_fin_price",dblSalesPriceTax_2.ToString());
//			itmQuote_Detail.setProperty("in_int_price",dblTax_2.ToString());			
//			itmQuote_Detail.apply("edit");

			dblSalesPriceTax_2 =0;
			dblTax_2 =0;
		
			break;
		case "2":
			dblSalesPriceTax_2 += dblSalesPriceTax_3;
			dblTax_2 += dblTax_3;
			
			sql = "Update [In_Invoice_Details] set ";
			sql += "in_sale_price_tax=" + dblSalesPriceTax_3.ToString();
			sql += ",in_tax=" + dblTax_3.ToString();
			sql += " where id='" + itmInvoice_Detail.getID() + "'";
			itmTemp = inn.applySQL(sql);

			//itmQuote_Detail.setProperty("in_fin_price",dblSalesPriceTax_3.ToString());
			//itmQuote_Detail.setProperty("in_int_price",dblTax_3.ToString());
			//itmQuote_Detail.apply("edit");

			dblSalesPriceTax_3 =0;
			dblTax_3 =0;
			
			break;
		case "3":
			dblSalesPriceTax_3 += dblSalesPriceTax;
			dblTax_3 += dblTax;
			
			break;
		default:
			break;
	}
}

//8.第一階的未稅單價(in_ori_sales_unit_price) = 未稅總價(in_sale_price) / 數量(in_qty), 但因為單價太小了,建議應取到幣別小數位數後兩位
sql = "Update [In_Invoice_Details] set ";
sql += "in_ori_sales_unit_price=Round(CAST((in_sale_price/in_qty) AS decimal(18,6))," + (intDec+2).ToString() + ") ";
sql += "  where source_id='" + this.getID() + "' and in_level=1 and in_qty>0";
itmTemp = inn.applySQL(sql);

sql = "Update [In_Invoice_Details]  set ";
sql += "in_ori_sales_price_t = REPLACE(CONVERT(varchar, CAST(in_ori_sales_price AS money), 1), '.00', ''),";
sql += "in_ori_sales_unit_price_t = REPLACE(CONVERT(varchar, CAST(in_ori_sales_unit_price_t AS money), 1), '.00', ''),";
sql += "in_ori_sales_unit_price_tax_t = REPLACE(CONVERT(varchar, CAST(in_ori_sales_unit_price_tax AS money), 1), '.00', ''),";
sql += "in_ori_sale_price_tax_t = REPLACE(CONVERT(varchar, CAST(in_ori_sale_price_tax AS money), 1), '.00', ''),";
sql += "in_ori_sum_sale_price_t = REPLACE(CONVERT(varchar, CAST(in_ori_sum_sale_price AS money), 1), '.00', ''),";
sql += "in_ori_sum_sale_price_tax_t = REPLACE(CONVERT(varchar, CAST(in_ori_sum_sale_price_tax AS money), 1), '.00', ''),";
sql += "in_sale_price_t = REPLACE(CONVERT(varchar, CAST(in_sale_price AS money), 1), '.00', ''),";
sql += "in_sale_price_tax_t = REPLACE(CONVERT(varchar, CAST(in_sale_price_tax AS money), 1), '.00', '')";

sql += " where SOURCE_ID='" + this.getID() + "'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Calculate_Invoice1-1' and [Method].is_current='1'">
<config_id>F2C56E16F5194B7E85377F8182E61F31</config_id>
<name>In_Calculate_Invoice1-1</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
