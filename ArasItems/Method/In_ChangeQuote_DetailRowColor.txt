/*
目的:顯示報價單細項的顏色, ㄧ、二、三階分別為不同的顏色
做法:
1.判斷本身的 Level 
2.依據Level分別為所有的欄位都上色
*/

//System.Diagnostics.Debugger.Break();
string[] arrColorCode = {"#92cdda","#cbe6bd","","",""};

for(int i=0;i<this.getItemCount();i++)
{
	Item thisItem = this.getItemByIndex(i);
	string css="";

	string strLevel = thisItem.getProperty("in_level","3");
	XmlNodeList nodes = thisItem.dom.SelectSingleNode("//Item").ChildNodes;
	foreach(XmlNode n in nodes)
	{
		string strColorCode = arrColorCode[Int32.Parse(strLevel)-1];
		if(strColorCode!="")
			css += "." + n.Name + " {background-color: " + strColorCode + " } ";
	}
	thisItem.setProperty("css",css);
	thisItem = thisItem.apply();
	
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ChangeQuote_DetailRowColor' and [Method].is_current='1'">
<config_id>26A60CFACE8C4F4A84FAC178BFFBB3D0</config_id>
<name>In_ChangeQuote_DetailRowColor</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
