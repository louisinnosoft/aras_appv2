//System.Diagnostics.Debugger.Break();
/*
目的:創建一個 Other 的 In_Criteria
傳入參數:<in_itemtype>itemtype_id</in_itemtype>
做法:
1.若有已存在相同的 itemtype 的 other criteria,則直接return error
2.創建一個新的 other criteria
*/
Innovator inn = this.getInnovator();
string aml = "";
string strItemtype = this.getProperty("in_itemtype","");
aml = "<AML>";
aml += "<Item type='In_Criteria' action='get'>";
aml += "<in_itemtype>" + strItemtype + "</in_itemtype>";
aml += "<in_type>others</in_type>";
aml += "</Item></AML>";

Item itmCriteria = inn.applyAML(aml);

if(!itmCriteria.isError())	
	return null;


aml = "<AML>";
aml += "<Item type='ItemType' action='get' select='label'>";
aml += "<id>" + strItemtype + "</id>";
aml += "</Item></AML>";

Item itmItemType = inn.applyAML(aml);
	
aml = "<AML>";
aml += "<Item type='In_Criteria' action='add'>";
aml += "<in_itemtype>" + strItemtype + "</in_itemtype>";
aml += "<in_type>others</in_type>";
aml += "<in_name>" + itmItemType.getProperty("label","") + " others</in_name>";
aml += "</Item></AML>";

itmCriteria = inn.applyAML(aml);

return itmCriteria;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateOtherCriteria' and [Method].is_current='1'">
<config_id>6CBD82433D0C4467A5ACE3D94D66DA08</config_id>
<name>In_CreateOtherCriteria</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
