debugger;
var itmAct2 = parent.item;
//alert(itmAct2.getAttribute("id",""));

var strProjName = top.aras.getItemProperty(itmAct2, "in_proj_name");
var strAct2Name = top.aras.getItemProperty(itmAct2, "name");

item = top.aras.newItem("In_WorkOrder");
// top.aras.setItemProperty(item, "name",strProjName);
top.aras.setItemProperty(item, "name",strProjName + "-" + strAct2Name);
top.aras.itemsCache.addItem(item);

var strAct2DateStartSched = top.aras.getItemProperty(itmAct2, "date_start_sched");
var strAct2DateDueSched = top.aras.getItemProperty(itmAct2, "date_due_sched");
var strAct2WorkEst = top.aras.getItemProperty(itmAct2, "work_est");

var WorkOrderDetailId = top.aras.getItemFromServerByName("RelationshipType", "In_WorkOrder_Detail", "id").node.getAttribute('id');

var relWorkOrderDetail = top.aras.newRelationship(WorkOrderDetailId, item);
top.aras.setItemProperty(relWorkOrderDetail, "related_id", itmAct2.getAttribute("id",""));

top.aras.setItemProperty(relWorkOrderDetail, "in_act2_start_sched", strAct2DateStartSched);
top.aras.setItemProperty(relWorkOrderDetail, "in_act2_due_sched", strAct2DateDueSched);
top.aras.setItemProperty(relWorkOrderDetail, "in_work_est", strAct2WorkEst);

top.aras.uiReShowItemEx(item.getAttribute("id"), item, "tab view");
top.aras.uiShowItemEx(item, "tab view");
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CreateWorkOrder' and [Method].is_current='1'">
<config_id>A7983A552E7B4B92B28F2C87E1197E65</config_id>
<name>In_CreateWorkOrder</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
