//當使用者刪除專案任務回報時，由系統依據工作產出，自動移除預算表的中相對應的生產費實際內容。
//in_DeleteProduceActCosts
//Activity2 Deliverable 的 onDelete
//System.Diagnostics.Debugger.Break(); 

Innovator inn = this.getInnovator();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
 
//先取得Propject的資訊:ID,in_isbudget
string aml="";
Item q;

string Act2ID = this.getProperty("source_id");
Item Act2 = inn.getItemById("Activity2",Act2ID);
string proj_num = Act2.getProperty("proj_num","");

aml = "<AML>";
aml += "<Item type='Project' action='get' select='id,in_isbudget'>";
aml += "<project_number>" + proj_num + "</project_number>";
aml += "</Item>";
aml += "</AML>";
q = inn.applyAML(aml);

string Prj_ID = q.getProperty("id");
string in_isbudget = q.getProperty("in_isbudget","");

if(in_isbudget=="")
{
	if(PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	return this;
}

aml = "<AML>";
aml += "<Item type='In_Budget' action='get' select='id,item_number' orderBy='created_on DESC' maxRecords='1'>";
aml += "<in_refproject>";
aml += Prj_ID;
aml += "</in_refproject>";
aml += "<state>Active</state>";
aml += "</Item>";
aml += "</AML>";
q = inn.applyAML(aml);

if(q.getItemCount()==0 || q.isError())
{
	if(PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
     throw new Exception("本專案目前無可用的預算表,無法計算預算表的生產費");
}

string BudgetID = q.getID();

//判斷 Budget 目前的鎖定狀態
if(q.fetchLockStatus()!=0)
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception("無法編輯" + q.getProperty("item_number","") + " 編號 預算表");
}

//獲取work_identity 的 User


aml = "<AML>";
aml += "<Item type='In_Budget_RD_ActCost' action='delete' where=\"[in_act2del_id]='" + this.getID() + "'\">";
aml += "</Item>";
aml += "</AML>";

q= inn.applyAML(aml);

string sql = "";
sql = "delete from [innovator].[Activity2_Deliverable] where id='" + this.getID() + "'";
q= inn.applySQL(sql);
 if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_DeleteProduceActCosts' and [Method].is_current='1'">
<config_id>D574CA844AE0491F8EB9948FE6E9703F</config_id>
<name>In_DeleteProduceActCosts</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
