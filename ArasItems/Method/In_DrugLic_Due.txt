/*
目的:將搜尋到的藥證資料組合成email發出
做法:
1.使用AML查詢到三個月內到期的藥證
2.組合出HTML email 內容
3.發給育婷
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strDateProperty = "in_expirationdate";

	
try
{
	string Message = "";
	Item itmDrugLic;
	Item itmDrugLics;
	
	
	//逾期未申請
	aml = "<AML>";
	aml += "<Item type='In_Drug_Lic' action='get' where='DATEDIFF(day,GETDATE(),[In_Drug_Lic].[in_expirationdate])&lt;=0' orderBy='in_expirationdate'>";
	aml +="</Item></AML>";
	itmDrugLics = inn.applyAML(aml);
	
	Message = "發信日期:" + System. DateTime.Now.ToString("yyyy-MM-dd");
	
	if(!itmDrugLics.isError())
	{
		Message += "<br>逾期未申請:";
		Message += "<table border='1' width='80%' cellspacing='0' cellpadding='0'>";
		Message += "<tr>";
		Message += "<td>許可證字號</td>";
		Message += "<td>中文品名</td>";
		Message += "<td>藥證到期日</td>";
		Message += "</tr>";
		for(int i=0;i<itmDrugLics.getItemCount();i++)
		{
			itmDrugLic = itmDrugLics.getItemByIndex(i);
			Message += "<tr>";
			Message += "<td>" + itmDrugLic.getProperty("in_licno","") + "</td>";
			Message += "<td>" + itmDrugLic.getProperty("in_name","") + "</td>";				
			Message += "<td>" + itmDrugLic.getProperty("in_expirationdate","").Substring(0,10) + "</td>";
			Message += "</tr>";
		}
		Message += "</table>";
	}
	
	
	//一個月內到期
	aml = "<AML>";
	aml += "<Item type='In_Drug_Lic' action='get' where='DATEDIFF(month,GETDATE(),[In_Drug_Lic].[in_expirationdate])&lt;=1 and DATEDIFF(day,GETDATE(),[In_Drug_Lic].[in_expirationdate])&gt;0' orderBy='in_expirationdate'>";
	aml +="</Item></AML>";
	itmDrugLics = inn.applyAML(aml);
	if(!itmDrugLics.isError())
	{
		Message += "<br>一個月內到期:";
		Message += "<table border='1' width='80%' cellspacing='0' cellpadding='0'>";
		Message += "<tr>";
		Message += "<td>許可證字號</td>";
		Message += "<td>中文品名</td>";
		Message += "<td>藥證到期日</td>";
		Message += "</tr>";
		for(int i=0;i<itmDrugLics.getItemCount();i++)
		{
			itmDrugLic = itmDrugLics.getItemByIndex(i);
			Message += "<tr>";
			Message += "<td>" + itmDrugLic.getProperty("in_licno","") + "</td>";
			Message += "<td>" + itmDrugLic.getProperty("in_name","") + "</td>";				
			Message += "<td>" + itmDrugLic.getProperty("in_expirationdate","").Substring(0,10) + "</td>";
			Message += "</tr>";
		}
		Message += "</table>";
	}
	
	//三個月內到期
	aml = "<AML>";
	aml += "<Item type='In_Drug_Lic' action='get' where='DATEDIFF(month,GETDATE(),[In_Drug_Lic].[in_expirationdate])&lt;=3 and DATEDIFF(month,GETDATE(),[In_Drug_Lic].[in_expirationdate])&gt;1' orderBy='in_expirationdate'>";
	aml +="</Item></AML>";
	itmDrugLics = inn.applyAML(aml);
	if(!itmDrugLics.isError())
	{
		Message += "<br>三個月內到期:";
		Message += "<table border='1' width='80%' cellspacing='0' cellpadding='0'>";
		Message += "<tr>";
		Message += "<td>許可證字號</td>";
		Message += "<td>中文品名</td>";
		Message += "<td>藥證到期日</td>";

		Message += "</tr>";
		for(int i=0;i<itmDrugLics.getItemCount();i++)
		{
			itmDrugLic = itmDrugLics.getItemByIndex(i);
			Message += "<tr>";
			Message += "<td>" + itmDrugLic.getProperty("in_licno","") + "</td>";
			Message += "<td>" + itmDrugLic.getProperty("in_name","") + "</td>";				
			Message += "<td>" + itmDrugLic.getProperty("in_expirationdate","").Substring(0,10) + "</td>";
			Message += "</tr>";
		}
		Message += "</table>";
	}
	
	//六個月內到期
	aml = "<AML>";
	aml += "<Item type='In_Drug_Lic' action='get' where='DATEDIFF(month,GETDATE(),[In_Drug_Lic].[in_expirationdate])&lt;=6 and DATEDIFF(month,GETDATE(),[In_Drug_Lic].[in_expirationdate])&gt;3' orderBy='in_expirationdate'>";
	aml +="</Item></AML>";
	itmDrugLics = inn.applyAML(aml);
	if(!itmDrugLics.isError())
	{
		Message += "<br>六個月內到期:";
		Message += "<table border='1' width='80%' cellspacing='0' cellpadding='0'>";
		Message += "<tr>";
		Message += "<td>許可證字號</td>";
		Message += "<td>中文品名</td>";
		Message += "<td>藥證到期日</td>";

		Message += "</tr>";
		for(int i=0;i<itmDrugLics.getItemCount();i++)
		{
			itmDrugLic = itmDrugLics.getItemByIndex(i);
			Message += "<tr>";
			Message += "<td>" + itmDrugLic.getProperty("in_licno","") + "</td>";
			Message += "<td>" + itmDrugLic.getProperty("in_name","") + "</td>";				
			Message += "<td>" + itmDrugLic.getProperty("in_expirationdate","").Substring(0,10) + "</td>";
			Message += "</tr>";
		}
		Message += "</table>";
	}
	
	
	//aml ="<AML>";
	//aml += "<Item type='User' action='get'>";
	//aml += "<first_name>Innosoft</first_name>";
	//aml += "</Item></AML>";
	//Item itmUser = inn.applyAML(aml);
	
	aml ="<AML>";
	aml += "<Item type='Identity' action='get'>";
	aml += "<name>RA</name>";
	aml += "</Item></AML>";
	Item itmIdentity = inn.applyAML(aml);
	

	
	
	_InnH.SendEmail("藥證到期通知",Message,itmIdentity.getID());
	
	
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_DrugLic_Due' and [Method].is_current='1'">
<config_id>5AC0494F750341E9BB60BC9C5C49A403</config_id>
<name>In_DrugLic_Due</name>
<comments>inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
