/*
目的:將常用的ItemType都啟用 APP 機制
做法:
*/
string strMethodName = "In_EnableAppForItem_All";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strError = "";
string strOk = "";
//string strUserId = inn.getUserID();
//string strIdentityId = inn.getUserAliases();

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;
	
string[] ItemTypeNames = new string[] {"Document","Part","Express DCO","Project","ECN","ECR","Simple ECO","Activity2","Customer","Identity","User"};
for(int i=0;i<ItemTypeNames.Length;i++)
{
	Item AppItem = inn.getItemByKeyedName("ItemType",ItemTypeNames[i]);
	if(AppItem.isError())		
		strError += ItemTypeNames[i] + ",";
	else
	{
		AppItem.apply("In_EnableAppForItem");
		strOk += ItemTypeNames[i] + ",";
	}

}
	
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return inn.newResult("成功:" + strOk+ ",失敗:" + strError);


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_EnableAppForItem_All' and [Method].is_current='1'">
<config_id>58F34558CC4147D8A1779ABD99EF9CD6</config_id>
<name>In_EnableAppForItem_All</name>
<comments>將常用的ItemType都啟用 APP 機制</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
