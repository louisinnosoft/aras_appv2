
Innovator inn=this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string id=this.getProperty("itemid","no_data");

if(id=="no_data"){
    return inn.newItem();
}

try{
    Item identity=inn.getItemById("Identity",id);

    if(identity.getProperty("is_alias","0")=="1"){
        //Item user=inn.getItemById("User",identity.getProperty("in_user","no_data"));
		Item user=_InnH.GetUserByAliasIdentityId(id);
        identity.setProperty("cell",user.getProperty("cell",""));
        identity.setProperty("email",user.getProperty("email",""));
        
    }
            identity.setProperty("id.type","Identity");

    return identity;    
}catch(Exception ex){
    
    return inn.newItem();
    
}




#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Fetch_IdentityContactInfo' and [Method].is_current='1'">
<config_id>E1D2B4B4F5D84D658887634A51DF0B9B</config_id>
<name>In_Fetch_IdentityContactInfo</name>
<comments>取得Identity的連絡資訊，如果是alias，另外從in_user拉出資料。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
