// Loop through the "input" elements
var elements = document.getElementsByTagName("input");
var Field_Percent=["in_staff_rate","in_other_rate","in_outsource_rate","in_material_rate","in_total_rate","in_staff_rate_f","in_other_rate_f","in_outsource_rate_f","in_material_rate_f","in_total_rate_f"];



var Field_Comma = ["in_quote_fin_price","in_fin_price","in_tax","in_fin_price_tax","in_remaining_credit"];

var propVal;

for (var i=0; i<elements.length; i++){
 // The property name to use is the input name of the field
 var propName = elements[i].name;
 if(Field_Percent.indexOf(propName)>-1){
	//補上小數點後三位
	propVal = document.thisItem.getProperty(propName);
	if (propVal === undefined) {
	   propVal = "0";
	}
	var num = new Number(propVal);
	propVal = num.toFixed(1);
	propVal = propVal + "%";
	elements[i].value = propVal;	
 }
 
 if(Field_Comma.indexOf(propName)>-1){
	//補上千分位
	propVal = document.thisItem.getProperty(propName);
	if (propVal === undefined) {
	   propVal = "0";
	}
	propVal = propVal.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
	elements[i].value = propVal;	
 }

}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_FormatInvoiceForm' and [Method].is_current='1'">
<config_id>3D7998CC760A4F9AA180E6F95D23149D</config_id>
<name>In_FormatInvoiceForm</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
