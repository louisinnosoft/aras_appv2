//將數字加上千分位

//可更新純數字欄位,例如 輸入 in_fin_price_t, 會自動將數值更新至 in_fin_price_t
//!注意!,若要達到自動更新數字型欄位,請將 數值型欄位開在hidden2上,寬度可設為0
//若數值型欄位沒有開到hidden2,不會發出錯誤,但也不會更新數值型欄位

//in_FormatNumber
//debugger;
var propertyValue = getRelationshipProperty(relationshipID, propertyName);
regexp = new RegExp( "," , 'g');
propertyValue = propertyValue.replace(regexp, '');
var floatPropertyValue = propertyValue;
if(isNaN(propertyValue))
{
    
	alert(inn.applyMethod("In_Translate","<text>請輸入純數字</text>").getResult());
	
	setTimeout(function () {		
		gridApplet.edit_Experimental.set(relationshipID, colNumber);
	}, 0);
	return false;
}

propertyValue = propertyValue.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");

setRelationshipProperty(relationshipID, propertyName, propertyValue);

//更新數字型欄位
var floatPropertyName = propertyName.substring(0,propertyName.length-2);
if(getRelationshipProperty(relationshipID,floatPropertyName))
{
	setRelationshipProperty(relationshipID, floatPropertyName, floatPropertyValue);
}
return true;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_FormatNumber' and [Method].is_current='1'">
<config_id>B49D032D282144B1A030C067E47A3CD1</config_id>
<name>In_FormatNumber</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
