//System.Diagnostics.Debugger.Break();
//in_GetSearchResult
Innovator inn = this.getInnovator();
string criteria = this.getProperty("criteria");
string UserInfo = this.getProperty("userinfo","");
string r = "";
	Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
try
{

	r = _InnoApp.GetSearchResult(criteria);
	
}
catch (Exception ex)
{
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
	 r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);

}


return  inn.newResult(r);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetMyOpenTimeRecords' and [Method].is_current='1'">
<config_id>7B5154F7807C4CBBA6581598A493F1B8</config_id>
<name>In_GetMyOpenTimeRecords</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
