// //System.Diagnostics.Debugger.Break();

string strMethodName = "In_GetReTypeFieldValue";

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";
string strFieldValue = "";

Item itmR = this;

try
{
    string strItemName = itmR.getProperty("ItemName","");
    string strItemID = itmR.getProperty("ItemID","");
    string strReTypeName = itmR.getProperty("ReTypeName","");
    string strRelatedName = itmR.getProperty("RelatedName","");
    string strFieldName = itmR.getProperty("FieldValue","");
    
    aml  = "<AML>";
    aml += "<Item type='"+strItemName+"' action='get'>";
    aml += "<id>"+strItemID+"</id>";
    aml += "</Item></AML>";
    Item itmItemType = inn.applyAML(aml);
    
    aml = "<AML>";
    aml += "<Item type='"+strReTypeName+"' action='get'>";
    aml += "<source_id>"+itmItemType.getID()+"</source_id>";
    aml += "</Item></AML>";
    Item itmReTypes = inn.applyAML(aml);
    
    for(int i=0;i<itmReTypes.getItemCount();i++)
    {
        if(strRelatedName == "")
        {
            strFieldValue += itmReTypes.getItemByIndex(i).getProperty(strFieldName,"") + ",";
        }
        else
        {
            aml = "<AML>";
            aml += "<Item type='"+strRelatedName+"' action='get'>";
            aml += "<id>"+itmReTypes.getItemByIndex(i).getProperty("related_id","")+"</id>";
            aml += "</Item></AML>";
            Item itmRelated = inn.applyAML(aml);
            
            strFieldValue += itmRelated.getProperty(strFieldName,"") + ",";
        }
    }
    
    strFieldValue = strFieldValue.Trim(',');
    
    itmR = inn.newItem("r");
    itmR.setProperty("FieldValue",strFieldValue);
}
catch(Exception ex)
{
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
    
    string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
    
    string strError = ex.Message + "\n";
    
    if(aml!="")
    strError += "無法執行AML:" + aml  + "\n";
    
    if(sql!="")
    strError += "無法執行SQL:" + sql  + "\n";
    
    string strErrorDetail="";
    strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
    
    Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
    strError = strError.Replace("\n","</br>");
    
    throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetReTypeFieldValue' and [Method].is_current='1'">
<config_id>A2C5EE6BDDF24CD1B8CCA3CDDFFB98D8</config_id>
<name>In_GetReTypeFieldValue</name>
<comments>方法:取得表身的欄位值</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
