/*
目的:
做法:
*/
string strMethodName = "In_GetRel_N";
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = null;
	
try
{
	//itemtype:In_Identity_teamlist&where:[In_Identity_teamlist].[source_id]='DF492F54847C4D53B870C0326D0F216F'&source_itemtype:identity&source_config_id:DF492F54847C4D53B870C0326D0F216F&app:in_web_relationship
	string strRelItemType = this.getProperty("rel_itemtype","");
	string strSourceId = this.getProperty("source_id","");
	string strSourceItemType = this.getProperty("source_itemtype","");
	Item itmSourceItem = inn.newItem(strSourceItemType);
	itmSourceItem.setID(strSourceId);
	itmSourceItem.setAttribute("select","config_id");
	itmSourceItem = itmSourceItem.apply("get");
	
	string strSourceCfgId = itmSourceItem.getProperty("config_id","");
	
	string criteria = "itemtype:" + strRelItemType;
	criteria += "&where:[" + strRelItemType + "].[source_id]='" + strSourceId + "'";
	criteria += "&source_itemtype:" + strSourceItemType;
	criteria += "&source_config_id:" + strSourceCfgId + "&app:in_web_relationship";
	Innosoft.app _InnoApp = new Innosoft.app(inn);
	_InnoApp.AppVer="2";
	string r = _InnoApp.GetRel(criteria);
	
	XmlDocument xmlOld = new XmlDocument();
	xmlOld.LoadXml(r);
	//we
	XmlNode xnItem = xmlOld.SelectSingleNode("//response/result");
	XmlNode xnHeader = xmlOld.SelectSingleNode("//response/result/Header");
	if(xnHeader!=null)
		xnItem.RemoveChild(xnHeader);
	Item itmOldItem = inn.newItem();
	string strInnerXml = xnItem.InnerXml ;
	
	if(!strInnerXml.Contains("<AML>"))
		strInnerXml = "<AML>" + xnItem.InnerXml + "</AML>";
	itmOldItem.loadAML(strInnerXml);

    for(int i=0;i<itmOldItem.getItemCount();i++)
    {
        	Item itmTmp=itmOldItem.getItemByIndex(i);
        if(itmTmp.getType()=="")
            itmTmp.setType(itmTmp.getAttribute("itemtype",strRelItemType));
            //itmTmp.setType(strRelItemType);
    }
    itmR = itmOldItem;

	
}
catch(Exception ex)
{	
	
	
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;

   
	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetRel_N' and [Method].is_current='1'">
<config_id>8BC5E0B1AA7947A093E2D9FE26A70D2A</config_id>
<name>In_GetRel_N</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
