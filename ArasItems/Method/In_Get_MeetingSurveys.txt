//本方法由b.aspx使用。
//傳入參數： 
//  meeting_id=會議的id
//  surveytype : 要求的問卷的類型(ex:事前問卷、事後問卷...)
//  status : 問卷狀態(1:期間內且未填寫、2:已超時未填寫，基本上不會出現、3:已填寫)，這個參數主要是寫在網頁上回到前端時讓javascript辨別狀態用，會被寫成：inn_sur_sta/  tus。
//  muid : 關聯的會議使用者id
//本方法只負責取出題目及答案，確認是否在期間內由In_Check_MeetingSurveyActive進行。取得此連結時應當已進行過確認。


//string meeting_id=this.getProperty("meeting_id","6908863025B74A008F8A8C1A88E2FBBF");
//string surveytype=this.getProperty("surveytype","2");
//string status=this.getProperty("status","1");//主要是要寫回去給javascript判別狀態用，查詢沒有使用到這個值。

string muid=this.getProperty("muid","");
string meeting_id=this.getProperty("meeting_id","");
string surveytype=this.getProperty("surveytype","");
string status=this.getProperty("status");
Innovator inn=this.getInnovator();
Item result=inn.newItem();
result.setAttribute("type","In_Meeting");
result.setAttribute("action","get");
result.setID(meeting_id);
result=result.apply();

//補上上前端需要的數值
result.setProperty("in_surveytype",surveytype);
result.setProperty("muid",muid);

Item surveys=inn.newItem();
surveys.setAttribute("type","In_Meeting_Surveys");
surveys.setAttribute("action","get");
surveys.setAttribute("orderBy","[In_Meeting_Surveys].sort_order");
surveys.setProperty("source_id",meeting_id);
surveys.setProperty("in_surveytype",surveytype);

surveys=surveys.apply();
 int sCount=surveys.getItemCount();
for(int k=0;k<sCount;k++){
    Item tmp=surveys.getItemByIndex(k);
   // Item answer=inn.newItem();
//    answer.setAttribute("type","Inn_Meeting_Surveys_result");
 //   answer.setProperty("in_answer",tmp.getProperty("in_answer"));
  //  answer.setProperty("inn_question_id",tmp.getRelatedItemID());
    result.addRelationship(tmp.getRelatedItem());
    
}
result.setProperty("inn_label",this.getProperty("label"));

return result;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_MeetingSurveys' and [Method].is_current='1'">
<config_id>9013C82DB5EE4C23A928A0FE0DCDA0F3</config_id>
<name>In_Get_MeetingSurveys</name>
<comments>會議進行中或結束後取得問卷，本方法由b.aspx使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
