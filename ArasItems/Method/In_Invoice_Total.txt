/*
撰寫:JOE
目的:計算欄位中的數值並更新至表頭
做法:
1.使用SumRelationshipPropertyToSourceProperty
位置:onAfterAdd,onAfterUpdate
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string aml = "";
string sql = "";
int intToTal = 0;

aml = "<AML>";
aml += "<Item type='In_Invoice_Details' action='get'>";
aml += "<source_id>"+this.getID()+"</source_id>";
aml += "</Item></AML>";
Item itmInvoiceDetails = inn.applyAML(aml);
for(int i=0;i<itmInvoiceDetails.getItemCount();i++){
	Item itmInvoiceDetail = itmInvoiceDetails.getItemByIndex(i);
	
	int intMoneyTax = Convert.ToInt32(itmInvoiceDetail.getProperty("in_payment_o","0"));
	
	intToTal += intMoneyTax;
}
sql = "Update In_Invoice set ";
sql += "in_payment_o = '" + intToTal + "'";
sql += " where id = '" + this.getID() + "'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Invoice_Total' and [Method].is_current='1'">
<config_id>69A83B5B7BA84351B29C656B8037FBE4</config_id>
<name>In_Invoice_Total</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
