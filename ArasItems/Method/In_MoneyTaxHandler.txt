/*
目的:公用稅額計算
做法:
1.取得參數(表頭名稱,表頭ID,表身名稱,屬性名稱:含稅金額,未稅金額,稅額,計算基礎欄位(base))
2.取得表頭
3.取得幣別
4.取得發票方式
5.取得表身
6.如果基礎欄位是0,則將其他兩個欄位都清空(例如基礎欄位是 money_tax, 則將 money與tax 設定為0)
6.三者(含稅金額,未稅金額,稅額)都無值,則不做計算
7.表身:取得幣別
8.表身:取得發票方式
9.取得參數的值(含稅金額,未稅金額)
10.算出未稅金額(未稅金額=含稅金額/稅率)
11.算出含稅金額(含稅金額=未稅金額*稅率)
12.算出稅額(稅額=含稅金額-未稅金額)
13.更新各個欄位(含稅金額,未稅金額,稅額)
位置:物件動作(Method)
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

int intDecimal = 0;

Item itmItemType = null;
Item itmParent=null;


string aml = "";
string sql = "";

decimal dblMoney = 0;
decimal dblTaxCode = 0;
decimal dblMoneyTax = 0;
decimal dblTaxQuota = 0;

//1.取得參數(表頭名稱,表頭ID,表身名稱,屬性名稱:含稅金額,未稅金額,稅額)
string strParentType = this.getProperty("parent_type","");
string strParentID = this.getProperty("parent_id","");
string strRelType = this.getProperty("rel_type","");
string strMoneyTax = this.getProperty("money_tax","");
string strMoney = this.getProperty("money","");
string strTax = this.getProperty("tax","");
string strBase = this.getProperty("base","");

//2.取得表頭
itmItemType = inn.getItemById(strParentType,strParentID);
itmParent=itmItemType;


//3.取得幣別
if(itmItemType.getProperty("in_currency","") != ""){
    Item itmDecimal = inn.getItemById("In_Currency",itmItemType.getProperty("in_currency",""));
    intDecimal = Convert.ToInt32(itmDecimal.getProperty("in_decimal_places","0"));
}

//4.取得發票方式
if(itmItemType.getProperty("in_tax_code","") != ""){
    Item itmTaxCode = inn.getItemById("In_tax_code",itmItemType.getProperty("in_tax_code",""));
    dblTaxCode = Convert.ToDecimal(itmTaxCode.getProperty("in_sales_tax","0")) + 1;
}

if(strRelType != ""){
    
    //5.取得表身
    aml = "<AML>";
    aml += "<Item type='"+strRelType+"' action='get'>";
    aml += "<source_id>"+strParentID+"</source_id>";
    aml += "</Item></AML>";
    itmItemType = inn.applyAML(aml);
}

string strBasePropertyName = "";
if(strBase=="money_tax")
	strBasePropertyName = strMoneyTax;
else
	strBasePropertyName = strMoney;


//6.三者(含稅金額,未稅金額,稅額)都無值,則不做計算
for(int i=0;i<itmItemType.getItemCount();i++)
{
    Item itmItemTypes = itmItemType.getItemByIndex(i);
	
	//如果基礎欄位是0,則將其他兩個欄位都清空(例如基礎欄位是 money_tax, 則將 money與tax 設定為0)
	if(strBase!="")
	{
		if(itmItemTypes.getProperty(strBasePropertyName,"0") == "0")
		{
			sql = "Update " + itmItemTypes.getType() + " set ";
			sql += strMoney + "=0";
			sql += "," + strMoneyTax + "=0";
			sql += "," + strTax + "=0";
			sql += " where id = '" + itmItemTypes.getID() + "'";
			inn.applySQL(sql);
			continue;
		}
	}
	
    if(itmItemTypes.getProperty(strMoneyTax,"") == "0" && itmItemTypes.getProperty(strMoney,"") == "0" && itmItemTypes.getProperty(strTax,"") == "0")
    {
        
    }
    else{
        
        //7.表身:取得幣別
        if(itmItemTypes.getProperty("in_currency","") != ""){
            Item itmDecimal = inn.getItemById("In_Currency",itmItemTypes.getProperty("in_currency",""));
            intDecimal = Convert.ToInt32(itmDecimal.getProperty("in_decimal_places","0"));
        }
        
        //8.表身:取得發票方式
        if(itmItemTypes.getProperty("in_tax_code","") != ""){
            Item itmTaxCode = inn.getItemById("In_tax_code",itmItemTypes.getProperty("in_tax_code",""));
            dblTaxCode = Convert.ToDecimal(itmTaxCode.getProperty("in_sales_tax","0")) + 1;
        }
        
        //9.取得參數的值(含稅金額,未稅金額)
        dblMoneyTax = Convert.ToDecimal(itmItemTypes.getProperty(strMoneyTax,"0"));
        dblMoney = Convert.ToDecimal(itmItemTypes.getProperty(strMoney,"0"));
        
        //10.算出未稅金額(未稅金額=含稅金額/稅率)
        if(dblMoneyTax != 0){
            dblMoney = dblMoneyTax / dblTaxCode;
			dblMoney = Innosoft.InnUtility.Round(dblMoney, intDecimal);
        }else{
            
            //11.算出含稅金額(含稅金額=未稅金額*稅率)
            dblMoneyTax = dblMoney * dblTaxCode;
			dblMoneyTax = Innosoft.InnUtility.Round(dblMoneyTax, intDecimal);

        }
        
        //12.算出稅額(稅額=含稅金額-未稅金額)
        dblTaxQuota = dblMoneyTax - dblMoney;
        
        //13.更新各個欄位(含稅金額,未稅金額,稅額)
        sql = "Update " + itmItemTypes.getType() + " set ";
        sql += strMoney + "='" + dblMoney + "'";
        sql += "," + strMoneyTax + "='" + dblMoneyTax + "'";
        sql += "," + strTax + "= '" + dblTaxQuota + "'";
        sql += " where id = '" + itmItemTypes.getID() + "'";
        inn.applySQL(sql);
    }
}
//可以不需要重新GET就直接apply In_PaymentO2C, 因為In_PaymentO2C都是純SQL
//itmParent.apply("In_PaymentO2C");
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_MoneyTaxHandler' and [Method].is_current='1'">
<config_id>9258272D5D684996B6741991046646F9</config_id>
<name>In_MoneyTaxHandler</name>
<comments>公用稅額計算</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
