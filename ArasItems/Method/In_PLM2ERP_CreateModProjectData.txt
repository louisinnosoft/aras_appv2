//System.Diagnostics.Debugger.Break();
/*
目的:專案拋轉至啟動時,要發出XML給ERP,如果 in_is_integrated 為1,代表要更新專案,否則是建立專案,建立WBS
做法:
1.xml結構
<plm>
<connection>
<ws_url></ws_url>
</connection>
<param method='CreateProjectData' order='1'>
</param>
<param method='CreateWbsData' order='2'>
</param>
</plm>
1.如果這不是預算制的專案,則不需要拋轉
1.取得ERP的ACCESS定義資訊(in_variable: ERPAccess)
2.組合出專案的XML,
3.找到這個專案的預算表的所有會科,組成WBS的XML
4.建立queue[to_erp]
*/

Innovator inn = this.getInnovator();
string aml = "";
string str_r = "";


//1.如果這不是預算制的專案,則不需要拋轉
if(this.getProperty("in_isbudget","")!="1")
	return this;

string strPrjContent="<Document ><!--資料內容-->";
strPrjContent+="	<RecordSet id='1' ><!--單據資料RecordSet，id 為識別碼流水號 -->";
strPrjContent+="		<Master name='pja_file'> ";
strPrjContent+="			<Record>";
strPrjContent+="				<Field name='pja01' value='@PrjNum'/>";
strPrjContent+="				<Field name='pja02' value='@PrjName'/><!--專案名稱-->";
strPrjContent+="				<Field name='pja08' value='@UserNo' /><!--專案負責人(員工編號)-->";
strPrjContent+="				<Field name='pja07' value='JR01' /><!--專案類型(代碼)-->";
strPrjContent+="				<Field name='pja09' value='@UserDept' /><!--負責部門(代碼)-->";
strPrjContent+="				<Field name='pja11' value='@DateDueSched' /><!--預計完工日期,2015/11/30-->";
strPrjContent+="				<Field name='pja13' value='10000' /><!--專案預計總額-->";
strPrjContent+="				<Field name='pja16' value='80' /><!--預計總工作量-->";
strPrjContent+="				<Field name='pja17' value='2' /><!--預計工作量單位(1:人天2:人時)-->";
strPrjContent+="				<Field name='pja14' value='NTD' /><!--幣別(開窗)-->";
strPrjContent+="				<Field name='pja32' value='100000' /><!--計劃總收入-->";
strPrjContent+="				<Field name='pja33' value='50000' /><!--計劃總材料成本-->";
strPrjContent+="				<Field name='pja34' value='0' /><!--計劃總其他成本-->		";	
strPrjContent+="				<Field name='pj_state' value='@state' /><!--專案狀態-->		";				
		

if(this.getProperty("in_is_integrated","")!="1")	
{
	strPrjContent+="				<Field name='pja05' value='@CreatedDate' /> <!--專案立項日期-->";
	strPrjContent+="				<Field name='pja10' value='@DateStartSched' /><!--預計開始日期-->";
}


strPrjContent+="			</Record>";
strPrjContent+="		</Master>";
strPrjContent+="	</RecordSet>";
strPrjContent+="</Document >";

string strWBSContent="";
strWBSContent = "<RecordSet id='@Order' >	<!--單據資料RecordSet，id 為識別碼流水號 -->";
strWBSContent += "	<Master name='pjb_file'> ";
strWBSContent += "		<Record>";
strWBSContent += "			<Field name='pjb01' value='@PrjNum'/><!--專案代號-->";
strWBSContent += "			<Field name='pjb02' value='@AccountNo' /><!--會科(WBS編號) -->";
strWBSContent += "			<Field name='pjb03' value='@AccountName' /><!--WBS名稱-->";
strWBSContent += "			<Field name='pjb07' value='JW01' />	<!--WBS分類-->";
strWBSContent += "		</Record>";
strWBSContent += "	</Master>";
strWBSContent += "</RecordSet>";



Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
	Innosoft.InnovatorHelper _InnoH = new Innosoft.InnovatorHelper(inn);

	Item itmVariable = inn.getItemByKeyedName("In_Variable","ERPAccess");
	if(itmVariable.isError())
		throw new Exception("查無ERP連線設定,請確認設定參數:[ERPAccess]內容是否正確");
	string strAccess = itmVariable.getProperty("in_value","");
	if(strAccess=="")
		throw new Exception("ERP連線設定內容為空值,請確認設定參數:[ERPAccess]內容是否正確");
	
	//2.組合出專案的XML,
	strPrjContent = strPrjContent.Replace("@PrjNum",this.getProperty("in_number",""));
	strPrjContent = strPrjContent.Replace("@PrjName",this.getProperty("name",""));
	
	Item itmUser = _InnoH.GetUserByAliasIdentityId(this.getProperty("owned_by_id"));
	Item itmDept = _InnoH.GetDeptByAliasIdentityId(this.getProperty("owned_by_id"));
	if(itmDept.isError())
		throw new Exception(itmDept.getErrorString());
	strPrjContent = strPrjContent.Replace("@UserNo",itmUser.getProperty("user_no",""));
	strPrjContent = strPrjContent.Replace("@UserDept",itmDept.getProperty("in_number",""));
	
	DateTime dtDateDueSched = Convert.ToDateTime(this.getProperty("date_due_sched",""));
	DateTime dtDateStartSched = Convert.ToDateTime(this.getProperty("date_start_sched",""));
	DateTime dtCreatedOn = Convert.ToDateTime(this.getProperty("created_on",""));
	
	strPrjContent = strPrjContent.Replace("@DateDueSched",dtDateDueSched.ToString("yyyy/MM/dd"));	
	strPrjContent = strPrjContent.Replace("@CreatedDate",dtDateStartSched.ToString("yyyy/MM/dd"));
	strPrjContent = strPrjContent.Replace("@DateStartSched",dtCreatedOn.ToString("yyyy/MM/dd"));
	
	strPrjContent = strPrjContent.Replace("@state",this.getProperty("state",""));
		
	string strWBS = "";	
	if(this.getProperty("in_is_integrated","")!="1")	
	{
		//沒拋過才要拋會科	
		//3.找到SYS-ERP01預算表的所有會科,組成WBS的XML
		aml = "<AML>";
		aml += "<Item type='In_Budget_Aggregated' action='get' select='in_accounting(in_accounting_name,in_accounting_number,in_accounting_subjects)'>";
		aml += "<source_id>";
		aml += "<Item type='In_Budget' action='get'>";
		aml += "<item_number>SYS-ERP01</item_number>";
		aml += "</Item>";
		aml += "</source_id>";
		aml += "</Item>";
		aml += "</AML>";
		
		Item itmAccounts = inn.applyAML(aml);
		
		strWBS = "<Document><!--資料內容-->";
				
		for(int i=0;i<itmAccounts.getItemCount();i++)
		{
			string strTmpWBS = "";
			strTmpWBS= strWBSContent;
			Item itmAcctount = itmAccounts.getItemByIndex(i);
			Item itmAcctountingItem = itmAcctount.getPropertyItem("in_accounting");
			
			//aml = "<AML>";
			//aml += "<Item type='Filter Value' action='get'>";
			//aml += "<source_id>8416E2B5D50544E2AE9A2859F079AE36</source_id>"; //in_Second_subjects
			//aml += "<value>" + itmAcctount.getProperty("in_second_subjects","") + "</value>";
			//aml += "</Item></AML>";
			
			//Item itmAccountValue = inn.applyAML(aml);
		
			strTmpWBS =strTmpWBS.Replace("@Order",(i+1).ToString());
			strTmpWBS =strTmpWBS.Replace("@PrjNum",this.getProperty("in_number",""));
			//strTmpWBS =strTmpWBS.Replace("@AccountNo",itmAccountValue.getProperty("value",""));
			//strTmpWBS =strTmpWBS.Replace("@AccountName",itmAccountValue.getProperty("label",""));
			strTmpWBS =strTmpWBS.Replace("@AccountNo",itmAcctountingItem.getProperty("in_accounting_number",""));
			strTmpWBS =strTmpWBS.Replace("@AccountName",itmAcctountingItem.getProperty("in_accounting_name",""));
			
			strWBS += strTmpWBS;
		}
		strWBS +="</Document>";
	}
	//4.建立queue[to_erp]
	Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
	itmVariable = _InnH.GetInVariable("erp_integration");
	if(itmVariable.getProperty("erp","")=="workflow")
	{
		str_r = "<Request>";	
		str_r += strAccess;
		str_r += "<RequestContent>";
		str_r += strPrjContent;
		str_r += "</RequestContent>";
		str_r += "</Request>";	
		
		string strToFolderPath = itmVariable.getProperty("to_erp","");
		Innosoft.InnUtility.WriteTxtFile(strToFolderPath, "pja_file-" + System.DateTime.Now.ToString("yyyyMMddHHmmssff") + ".xml",str_r,false,true);		
	}
	else
	{
		string strProjectMethod = "";
		if(this.getProperty("in_is_integrated","")!="1")	
			strProjectMethod = "CreateProjectData";
		else	
			strProjectMethod = "ModProjectData";
			
		str_r = "<plm>";
			str_r += "<connection>";
			str_r += "<ws_url>" + inn.getItemByKeyedName("In_Variable","erp_ws_url").getProperty("in_value","") + "</ws_url>";	
			str_r += "</connection>";		
			
			str_r += "<param method='" + strProjectMethod + "' order='1'>";
				str_r += "<Request>";	
				str_r += strAccess;
				str_r += "<RequestContent>";
				str_r += strPrjContent;
				str_r += "</RequestContent>";
				str_r += "</Request>";	
			str_r += "</param>";
		if(this.getProperty("in_is_integrated","")!="1")	
		{		
			str_r += "<param method='CreateWbsData' order='2'>";
				str_r += "<Request>";	
				str_r += strAccess;
				str_r += "<RequestContent>";
				str_r +=  strWBS;
				str_r += "</RequestContent>";
				str_r += "</Request>";	
			str_r += "</param>";
		}
		str_r += "</plm>";
		
		Item itmQueue = inn.newItem("In_Queue","add");
		itmQueue.setProperty("in_source_itemtype","Project");
		itmQueue.setProperty("in_source_id",this.getID());
		itmQueue.setProperty("in_source_keyed_name",this.getProperty("keyed_name"));
		itmQueue.setProperty("in_type","plm2erp");
		itmQueue.setProperty("in_state","start");
		itmQueue.setProperty("in_inparam",str_r);
		itmQueue = itmQueue.apply();
		if(itmQueue.isError())
			throw new Exception(itmQueue.getErrorString());
		Item itmR = inn.applyMethod("In_ExecuteWSQueue","<queue_id>" + itmQueue.getID() + "</queue_id>");
		if(itmR.getResult()!="ok")
			throw new Exception(itmR.getResult());
	}
	
	
	string sql = "";
	sql = "Update [Project] set in_is_integrated='1' where id='" + this.getID() + "'";
	inn.applySQL(sql);
	
}
catch(Exception ex)
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.Message);
}
 if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_PLM2ERP_CreateModProjectData' and [Method].is_current='1'">
<config_id>0F9076D909974499B13FCFBECCFD8F84</config_id>
<name>In_PLM2ERP_CreateModProjectData</name>
<comments>inn erp</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
