/*
目的:將Part拋轉到ERP
說明:此為簡單版的ERP整合,可以自動產生出ECN, Part, BOM 檔案
做法:
1.提供PartID
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

Item itmQueue = _InnH.CreateQueue(this,"plm2erp-part","<part_id>" + this.getID() + "</part_id>",""); //(Item Context, string QueueType,string Param,string StateMethodName)


return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Part_PLM2ERP' and [Method].is_current='1'">
<config_id>1ED5C570062F4970989CFF1809D57B56</config_id>
<name>In_Part_PLM2ERP</name>
<comments>Inn erp</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
