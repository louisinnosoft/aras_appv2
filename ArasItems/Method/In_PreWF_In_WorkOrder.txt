string strMethodName = "In_PreWF_In_WorkOrder";
/*
目的:啟動In_WorkOrder 之前,將in_is_detail_complete欄位清空
*/
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;
	
try
{
	//將in_is_detail_complete欄位清空

	aml = "<AML>";
	aml += "<Item type='In_WorkOrder' action='edit' id='" + this.getID() + "'>";
	aml += "<in_is_detail_complete>0</in_is_detail_complete>";
	aml += "</Item></AML>";
	inn.applyAML(aml);
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_PreWF_In_WorkOrder' and [Method].is_current='1'">
<config_id>2F86233F99B44473AF4B6B7ED16075C6</config_id>
<name>In_PreWF_In_WorkOrder</name>
<comments>啟動In_WorkOrder 之前,將in_is_detail_complete欄位清空</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
