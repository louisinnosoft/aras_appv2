//System.Diagnostics.Debugger.Break();
/*目的:Queue狀態變化時,要做相對應的處理
位置:In_Queue Promote post-method
做法:
1.將Queue的狀態謄寫一份至source_item 的 in_queue_state 欄位內
2.依據狀態變更 in_start_time 與 in_end_time
3.若狀態為Pending,Failed,Closed,則要判斷是否發email
->若 in_email_error_message 有值,則套用 Email Message : In_QueueErrorMessage 
->若 in_email_message_notify 為 true ,則套用 Email Message : In_QueueNotify 
*/


Innovator inn = this.getInnovator();
string sql;
string aml;

//2.依據狀態變更 in_start_time 與 in_end_time
sql = "";
switch(this.getProperty("state",""))
{
	case "Start":
		sql = "Update In_Queue set in_start_time=null,in_end_time=null,in_return=null  where id='" + this.getID() + "'";
		break;
	case "In_Process":
		sql = "Update In_Queue set in_start_time='" + System.DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss") + "',in_end_time=null,in_return=null  where id='" + this.getID() + "'";
		break;
	case "Pending":
	case "Failed":
	case "Closed":
		Innosoft.InnUtility.AddLogToFolder("In_Queue:" + this.getProperty("item_number","") + "test","Info",CCO.Server.MapPath("../server/logs/" ));
		sql = "Update In_Queue set in_end_time='" + System.DateTime.Now.AddHours(-8).ToString("yyyy-MM-ddTHH:mm:ss") + "'  where id='" + this.getID() + "'";
		try{
			//判斷是否要發出email
			if(this.getProperty("in_email_error_message","")!="")
			{
				Item itmAdmin = inn.newItem("Identity","get");
				itmAdmin.setProperty("name", "Innovator Admin");
				itmAdmin = itmAdmin.apply();
				
				Item email_msg = this.newItem("Email Message","get");
				email_msg.setProperty("name","In_QueueErrorMessage");
				email_msg =email_msg.apply();

				if( this.email( email_msg, itmAdmin) == false )
				{			
					Innosoft.InnUtility.AddLogToFolder("In_Queue:" + this.getProperty("item_number","") + "無法發出email給" + itmAdmin.getProperty("name",""), "Info",CCO.Server.MapPath("../server/logs/" ));
					int i=0;
				}
			}
			
			Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
			if(this.getProperty("in_email_message_notify","")=="1")
			{
				string ToIdentityId=this.getProperty("owned_by_id","");
				if(ToIdentityId=="")
				{
					ToIdentityId = _InnH.GetIdentityByUserId(this.getProperty("created_by_id")).getID();			
				}
				Item email_msg = this.newItem("Email Message","get");
				email_msg.setProperty("name","In_QueueNotify");
				email_msg =email_msg.apply();
				
				Item itmToIdentity = inn.getItemById("Identity",ToIdentityId);

				if( this.email( email_msg, itmToIdentity) == false )
				{
					Innosoft.InnUtility.AddLogToFolder("In_Queue:" + this.getProperty("item_number","") + "無法發出email給" + itmToIdentity.getProperty("name",""), "Info",CCO.Server.MapPath("../server/logs/" ));
					int k=0;
				}
			}
		}
		catch(Exception ex)
		{
			Innosoft.InnUtility.AddLogToFolder("In_Queue:" + this.getProperty("item_number","") + "無法發出email" + ex.InnerException==null?ex.Message:ex.InnerException.Message,"Info",CCO.Server.MapPath("../server/logs/" ));
		}
		
		break;
	default:
		break;
	}

if(sql!="")
	inn.applySQL(sql);

//1.將Queue的狀態謄寫一份至source_item 的 in_queue_state 欄位內
//先判斷來源物件是否有挖好 in_queue_state欄位
aml = "<AML>";
aml += "<Item type='Property' action='get' select='id'>";
aml += "<source_id>";
aml += "<Item type='itemtype' action='get' select='id'>";
aml += "<name>" + this.getProperty("in_source_itemtype","") + "</name>";
aml += "</Item>";
aml += "</source_id>";
aml += "<name>in_queue_state</name>";
aml += "</Item></AML>";

Item itmProperty = inn.applyAML(aml);

if(!itmProperty.isError())
{
	sql = "update [" + this.getProperty("in_source_itemtype").Replace(" ","_") + "] set in_queue_state='" + this.getProperty("state","") + "' where id='" + this.getProperty("in_source_id","") + "'";

	Item itmSourceItem = inn.applySQL(sql);
}
//執行in_state_method
if(this.getProperty("in_state_method","")!="")
{
	Item itmThis = this.apply(this.getPropertyAttribute("in_state_method","keyed_name"));
}


return this;



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Queue_StateChanged' and [Method].is_current='1'">
<config_id>A3474BA3B1F94F5BBA2A3F8CB2368CDD</config_id>
<name>In_Queue_StateChanged</name>
<comments>Drug-Queue</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
