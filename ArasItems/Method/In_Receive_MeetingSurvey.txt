Innovator inn=this.getInnovator();
//接收會議中回傳的問卷答案。
//System.Diagnostics.Debugger.Break();
//處理傳入的各項資料
string meeting_id=this.getProperty("meeting_id","no_data");//對應的會議物件編號
string muid=this.getProperty("muid");//填答的使用者ID
string surveytype=this.getProperty("surveytype","no_data");
string param=this.getProperty("parameter","no_data"); //取得傳入的param，是escape過的form-data


if(meeting_id=="no_data" || muid=="no_data" || surveytype=="no_data" || param=="no_data"){
    return inn.newResult("0");//資料不全，傳回0
}

string[] pms=param.Split('&');//處理傳入的parameter
string action="add";//要採取的action，如果使用者已存在會被改成merge


//建立答案卷並將答案填入
for(int p=0;p<pms.Length;p++){
    string[] kvs=pms[p].Split('=');
    string surveyID=kvs[0];
    string[] answers=kvs[1].Split(',');
    for(int e=0;e<answers.Length;e++){
            //建立In_Meeting_Surveys_result物件
            Item answerSheet=inn.newItem();
            answerSheet.setAttribute("type","In_Meeting_Surveys_result");
                answerSheet.setAttribute("action","add");
                answerSheet.setProperty("source_id",meeting_id);
                answerSheet.setProperty("related_id",kvs[0]);
                answerSheet.setProperty("in_answer",answers[e]);
                answerSheet.setProperty("in_participant",muid);
                answerSheet.setProperty("in_surveytype",surveytype);
                answerSheet=answerSheet.apply();
            }
    
}

    /*建立回答的標準動作
            Item answerSheet=inn.newItem();
            answerSheet.setAttribute("type","In_Meeting_Surveys_result");
            answerSheet.setAttribute("action","add");
            answerSheet.setProperty("source_id",meeting_id);
            answerSheet.setProperty("related_id",surveyID);
            answerSheet.setProperty("in_answer",answer);
            answerSheet.setProperty("in_participant",mUser.getID());
            Item last=answerSheet.apply();   
*/






//正常結束回傳1
return inn.newResult("1");



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Receive_MeetingSurvey' and [Method].is_current='1'">
<config_id>A5078A733821470BAD3A9C60077122E6</config_id>
<name>In_Receive_MeetingSurvey</name>
<comments>接收回傳的問卷答案，由InnoJSON.ashx使用</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
