//System.Diagnostics.Debugger.Break();
//文件更新後,如果流程狀態 in_wfp_state != "Active",就將欄位都清空
Innovator inn = this.getInnovator();
string strLogIns = Aras.Server.Security.Permissions.Current.IdentitiesList;
string sql = "";

if(this.getProperty("in_wff_id")!=null)
{
    sql = "Update [In_WorkflowForm] set owned_by_id='"+this.getProperty("owned_by_id","null")+"',managed_by_id='"+this.getProperty("managed_by_id","null")+"',created_by_id='"+this.getProperty("created_by_id","null")+"',team_id='"+this.getProperty("team_id","null")+"' where id='" + this.getProperty("in_wff_id") + "'";
	inn.applySQL(sql);
}

// 如果不是admin 就將欄位清空
if(strLogIns.Contains("2618D6F5A90949BAA7E920D1B04C7EE1"))
	return this;

if(this.getType() != "in_proposal")
{

if(this.getProperty("in_wfp_state","") != "Active"){
	sql = "Update [" + this.getType().Replace(" ","_") + "] set in_wff_id=null,in_wfp_id=null,in_wfp_state=null,in_current_activity=null where id='" + this.getID() + "'";
	inn.applySQL(sql);
}

}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_RemoveWFFProperty' and [Method].is_current='1'">
<config_id>F1737AB9C9594F07B7503C9D1CA9E662</config_id>
<name>In_RemoveWFFProperty</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
