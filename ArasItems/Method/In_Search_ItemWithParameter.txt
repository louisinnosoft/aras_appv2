/*
...
用傳入的條件進行搜尋，並將搜尋結果放入Relationship，以"Inn_SearchResult"傳回結果。
如果有結果本方法會忽略Mapper使用的param: page 、method、embedded、in_do_search(自用參數)

自用參數：
    in_do_search :傳入1或0，是否進行搜尋，若為0，則回傳空物件，若為1，回傳含搜尋結果物件。
    inn_original_query_string :原始查詢字串，直接寫回前端。
    inn_result_sort: 傳入desc或asc，搜尋時排序方式
    inn_sort_by: 傳入屬性名稱，以哪個屬性進行排序
Mapper使用參數：
    page    
    method  
    embedded

參數傳入結構與其他使用c.aspx的物件相同。但是這次會做分析但是會分析字串。

<屬性名稱>查詢種類:查詢值,查詢種類:查詢值...</屬性名稱>

查詢種類：見Aras文件裡的Condition，會被填入property標籤中的Condition屬性。
    種類：eq、ne、ge、le、gt、lt、like、not like、is、is null、is not null，空白字元以_(底線)取代

查詢值:任何要查詢的值，注意如果要使用 "*"，需在傳入時處理完成，這邊不處理星號或任何格式問題。

<屬性名稱>屬性值<屬性名稱>

以搜尋Identity為例，

搜尋: 姓名(name)中有"王"字、不含有"護士"的Identity，且為別名(is_alias)的Identity，收到的AML如下：
會收到如下的AML：

<Item type="Method" action="In_Search_ItemWithParameter">
    <method>In_Search_ItemWithParameter</method><!--Mapper使用的參數，不會被傳入搜尋-->
    <page>SomePage.html<page><!--Mapper使用的參數，不會被傳入搜尋-->
    <q_type>Identity</q_type>
    <name>like:*王*,not_like:*護士*<name>除了內容為搜尋字串以外，其他地方
    <is_alias>1</is_alias>
</Item>


除了內容為搜尋字串以外，其他地方都跟一般查詢相同。不符合搜尋字串規則的"內容"(如範例中的is_alias)不會被處理。
再次注意要使用"*"的話傳入前就要處理好。

*/

Innovator inn=this.getInnovator();
Item rst=null;
Item opt=null; //Inn_Option
List<string> fetchables=null;
List<string> listFetchables=null;


string queryItemType=this.getProperty("q_type","no_data");
string sortMethod=this.getProperty("inn_result_sort","desc");
string sortBy=this.getProperty("inn_sort_by","created_on");

//處理Inn_Option
opt=inn.newItem("Inn_Option","Whatever");
opt.setProperty("inn_searchtype",queryItemType);



if(this.getProperty("in_do_search","0")=="0"){
    rst=inn.newItem();
    rst.setProperty("id.type",queryItemType);
    rst.addRelationship(opt);
    return rst;}
if(queryItemType=="no_data"){return inn.newError("Missing \"q_type\" parameter");}
Item user=inn.getItemById("User",inn.getUserID());
Item sample=inn.newItem(queryItemType,"get");
sample.setAttribute("maxRecords","1");
sample.setAttribute("pagesize","1");
sample.setAttribute("page","1");
sample=sample.apply();



System.Text.RegularExpressions.Regex queryPattern=
new System.Text.RegularExpressions.Regex("(eq|ne|ge|le|gt|lt|like|not_like|is|is_null|is_not_null):[^,]*");
Item fetchList=inn.newItem("Method","In_Get_PropertyFetchList");
fetchList.setProperty("typeToFetch",queryItemType);
Item fetchListQ=fetchList.apply();
fetchables=new List<string>(fetchListQ.getResult().Split(new char[]{','}));

fetchList.setProperty("datatype","list");
Item fetchListLabel=fetchList.apply();
listFetchables=new List<string>(fetchListLabel.getResult().Split(new char[]{','}));

System.Xml.XmlNodeList childs=this.node.ChildNodes;
Item query=inn.newItem(queryItemType,"get");
//設定Query的參數，最大300筆，由新到舊。
query.setAttribute("maxRecords","300");
query.setAttribute("orderBy",sortBy+" "+sortMethod);
char[] splitter=new char[]{':'};
    Item orNode=query.newOR();
    


foreach(XmlNode node in childs ){
    string propertyName=node.Name;
    if(propertyName =="method" || propertyName=="page" ||propertyName=="in_do_search" || propertyName=="q_type" ||propertyName=="embedded" || propertyName=="inn_original_query_string"){continue;}//跳過mapper用跟自用的parameter
    System.Text.RegularExpressions.MatchCollection queryUnits=queryPattern.Matches(node.InnerText);
    propertyName=node.Name;
    if(queryUnits.Count!=0){
        

        foreach(System.Text.RegularExpressions.Match match in queryUnits){
            string[] queryKeyValuePair=match.Value.Split(splitter);//input= "key:value"=>output =[key,value]
            queryKeyValuePair[0]=queryKeyValuePair[0].Replace("_"," ");
            Item tmp=null;
            if(queryKeyValuePair[1]==null || queryKeyValuePair[1]==""){continue;}

            
            switch(queryKeyValuePair[0]){
                
                case "eq":tmp=orNode.newOR();break;
                case "ne":tmp=orNode.newAND();break;
                case "ge":tmp=orNode.newOR();break;
                case "gt":tmp=orNode.newOR();break;
                case "lt":tmp=orNode.newOR();break;
                case "like":tmp=orNode.newOR();break;
                case "not like":tmp=orNode.newAND();break;
                case "is":tmp=orNode.newOR();break;
                case "is null":tmp=orNode.newAND();break;
                case "is not null":orNode=query.newAND();break;
                default:query.setProperty(node.Name,node.InnerText);break;
                
            }

            if(tmp==null){continue;}
            if(fetchables.Contains(propertyName) &!sample.isError()){
                Item referred=inn.newItem(sample.getPropertyAttribute(propertyName,"type",""),"get");
                referred.setProperty("keyed_name",queryKeyValuePair[1]);
                referred.setPropertyCondition("keyed_name","like");
                tmp.setPropertyItem(propertyName,referred);    
                
                
            }else{
                tmp.setProperty(propertyName,queryKeyValuePair[1]);
                tmp.setPropertyCondition(propertyName,queryKeyValuePair[0]);
                }
            
            }
        }
        else{
        query.setProperty(node.Name,node.InnerText);
    }
                    


}

    rst=inn.newItem();
var IsAllCompany = "";

    //檢查是否為屬於AllCompany群組
    

            if (IsAllCompany == "")
            {
                
                var d_aml = "<AML>";
                d_aml += "<Item type='Identity' action='get'>";
                d_aml += "<name>AllCompany</name>";
                d_aml += "</Item></AML>";
                Item itmAllCompany = inn.applyAML(d_aml);
                
                if (!itmAllCompany.isError())
                {
                    var strAllCompanyId = itmAllCompany.getID();
                    string compnayId=inn.applyMethod("In_GetIdentityList", "<user_id>" + inn.getConnection().getUserID() + "</user_id>").getResult();
                    if (compnayId.Contains(strAllCompanyId))
                    {
                        IsAllCompany = "true";
                    }
                    else
                    {
                        IsAllCompany = "false";
                    }
                }
                else
                {
                    IsAllCompany = "no_use";
                }

            }
    if(IsAllCompany=="false" || IsAllCompany==""){
        query.setProperty("in_company",user.getProperty("in_company","no_data"));    
    }
    
    query=query.apply();
    int resultCode=query.getItemCount();
    if(resultCode>0  || !query.isError()){
        string typeId=query.getItemByIndex(0).getAttribute("typeId","");
        
        Dictionary<string,Item> lists=new Dictionary<string,Item>();
        foreach(string listName in listFetchables){
			if(listName==""){continue;}
            Item qRst = inn.newItem("Property","get");
            qRst.setProperty("source_id",typeId);
            qRst.setProperty("name",listName);
            qRst.setAttribute("queryType","Latest");
            qRst=qRst.apply();
            
            
            Item list=inn.getItemById("list",qRst.getProperty("data_source"));
            lists.Add(listName,list);
        }
        
        Item labelMatcher=inn.newItem("Method","In_Find_MatchedLabel");
        int count=query.getItemCount();
        for(int x=0;x<count;x++){
            Item tmp=query.getItemByIndex(x);
            //tmp = tmp.apply("In_AppendExtraProperties");
            tmp.setAttribute("type","Inn_SearchResult");
            
            
            foreach(string propName in listFetchables){
				if(propName==""){continue;}
                string propValue=tmp.getProperty(propName,"");
                if(propValue==""){continue;}
                labelMatcher.setProperty("property_value",propValue);
                labelMatcher.setPropertyItem("collection",lists[propName]);
                Item value=labelMatcher.apply();
                string itValue=value.getResult();
                tmp.setProperty(propName+".listlabel",itValue);
                
            }

            foreach(string propName in fetchables  ){
                if(propName==""){continue;}
                string propType=tmp.getPropertyAttribute(propName,"type","");
                if(propType==""){continue;}

                Item deepItem=inn.getItemById(propType,tmp.getProperty(propName,""));
                if(deepItem==null){continue;}
                tmp.setProperty(propName+".keyed_name",deepItem.getProperty("keyed_name",""));
                tmp.setProperty(propName+".name_name",deepItem.getProperty("name",""));
            }
            
            
                rst.addRelationship(tmp);    

        }
            

        
    }else{
            rst=inn.newItem();

    }
    rst.addRelationship(opt);
    rst.setProperty("inn_original_query_string",this.getProperty("inn_original_query_string",""));
    rst.setProperty("inn_searchtype",queryItemType);
    rst.setType("method");
    rst = rst.apply("In_AppendExtraProperties");
return rst;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Search_ItemWithParameter' and [Method].is_current='1'">
<config_id>FE88CBCB9B6341F68F6401ABAE101014</config_id>
<name>In_Search_ItemWithParameter</name>
<comments>用傳入的條件進行搜尋。以Inn_SearchResult傳回搜尋結果。支援Mapper與InnoJSON的AML模式。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
