//這個方法被掛載在In_Meeting的物件動作上，this的狀態是所有relationships都抓完的狀態。

//System.Diagnostics.Debugger.Break();
Innovator inn=this.getInnovator();

Item meetingUsers=this.getRelationships("In_Meeting_User");
string errLogs="";
string url=this.getProperty("in_register_url","no_data");
int muCount=meetingUsers.getItemCount();
for(int m=0;m<muCount;m++){
    Item tmp=meetingUsers.getItemByIndex(m);
    string toName=tmp.getProperty("in_name","no_data");
    string toEmail=tmp.getProperty("in_mail","no_data");
    string invite=tmp.getProperty("in_invite","0");
    string invited=tmp.getProperty("in_invite_mail","no_data");
    if(invite=="0" || invited!="no_data"){
        continue;
    }
    if(toName=="no_data" && toEmail=="no_data"){
        string errLog="Emailing error missing email or name on In_Meeting_User  id="+tmp.getID();
        errLogs+=errLog;
        CCO.Utilities.WriteDebug(System.DateTime.Now.ToString("yyyy-MM-ddTss:mm"),errLog);
        continue;
    }
    
    
    // add from,to System.Net.Mail.MailAddresses
   System.Net.Mail.MailAddress from = new System.Net.Mail.MailAddress("tanchi@innosoft.com.tw", "MeetingAdmin");
   System.Net.Mail.MailAddress to = new System.Net.Mail.MailAddress(toEmail, toName);
   System.Net.Mail.MailMessage myMail = new System.Net.Mail.MailMessage(from, to);

   // add ReplyTo
   System.Net.Mail.MailAddress replyTo = new System.Net.Mail.MailAddress("no-reply@example.com");
   myMail.ReplyToList.Add(replyTo);

   // set subject and encoding
   myMail.Subject = this.getProperty("in_title","")+"邀請函";
   myMail.SubjectEncoding = System.Text.Encoding.UTF8;

   // set body-message and encoding
   myMail.Body = toName+@" 先生/小姐您好：<br>最近公司有舉辦一場研討會你可能會有興趣。網址如下：<br>
如有興趣請直接在網頁上填寫基本資料並按下送出即可。<br>"+url;
   myMail.BodyEncoding = System.Text.Encoding.UTF8;
   // text or html
   myMail.IsBodyHtml = true;
    tmp.setAttribute("action","edit");
    tmp.setProperty("in_invite_mail",System.DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss"));
    tmp.apply();
   CCO.Email.SetupSmtpMailServerAndSend(myMail);

    
}


 

   

if(errLogs!=""){
    return inn.newError("Part of Email were not send Correctly ,check log for detail");
}else{
    return inn.newResult("ok");
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Send_MeetingInvitation' and [Method].is_current='1'">
<config_id>DDC0C2A910214F0891BEA284105FE077</config_id>
<name>In_Send_MeetingInvitation</name>
<comments>(會議物件)發送邀請函，內部使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
