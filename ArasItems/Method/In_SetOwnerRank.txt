/*
目的:將 owner 對應的 user 的 rank 更新到 in_rank與in_rank_name 欄位中
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;

try
{
	if(this.getProperty("owned_by_id","")=="")
		return this;

	Item itmUser = _InnH.GetUserByAliasIdentityId(this.getProperty("owned_by_id",""));
	string strUserRankId = itmUser.getProperty("in_rank","");//User的職級
	string strUserCompanyId = itmUser.getProperty("in_company","");//User的公司別
	string strRankName = itmUser.getPropertyAttribute("in_rank","keyed_name", "");
	string strTimeRecordCompany = this.getProperty("in_company","");//工時的公司別
	string strTimeRecordRank = this.getProperty("in_rank", "");//工時的職級

//判斷工時職級是否為空,若為空則壓上Owner的職級

	if(strTimeRecordRank == ""){
	    sql = "Update [" + this.getType().Replace(" ","_") + "] set ";
        sql += "in_rank = '" + strUserRankId + "'";
		sql += ",in_rank_name = '" + strRankName  + "'";
        sql += " where id = '" + this.getID() + "'";
    	inn.applySQL(sql);
    	
    	//為了給後面的In_WorkTime_Control檢查,因此要setProperty
    	this.setProperty("in_rank",strUserRankId);
    	this.setProperty("in_rank_name",strRankName);
	}
	else
	{
		strRankName  = this.getPropertyAttribute("in_rank", "keyed_name","");
		sql = "Update [" + this.getType().Replace(" ","_") + "] set ";
		sql += " in_rank_name = '" + strRankName  + "'";
		sql += " where id = '" + this.getID() + "'";
		inn.applySQL(sql);
		//為了給後面的In_WorkTime_Control檢查,因此要setProperty
    	this.setProperty("in_rank_name",strRankName);
	}
	
	
	

	
	//Innosoft.InnUtility.AddLog(this.getID().ToString(), "Hi");

	/*
	sql = "Update [" + this.getType().Replace(" ","_") + "] set in_rank=" + strRankId + " ,in_rank_name = '" + strRankName  + "' ,in_company = '" + strRankCompany + "' where id='" + this.getID() + "'";
	inn.applySQL(sql);
	*/
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetOwnerRank' and [Method].is_current='1'">
<config_id>13E59DBB33334BED893CB7E0BC0E89B4</config_id>
<name>In_SetOwnerRank</name>
<comments>將 owner 對應的 user 的 rank 更新到 in_rank與in_rank_name 欄位中</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
