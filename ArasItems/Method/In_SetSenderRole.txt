/*
目的:依據Creator判斷 in_sender_role 要填入甚麼值,用以辨識流程中的分叉路線
做法:
1.抓到Creator 所屬的所有identity
2.目前定義的角色是:PM,PD,業務部,代工部,如果同時屬於兩個部門,則會跳出訊息,讓使用者自行挑選

List內容需參考:In_Project_filing_Sender_Role

Identity ID:
PM:4E70FC163DF24D3AAC05188C09A27022
PD:39E66592D4DF4AD780062D9B763A0FB5
業務部:9D7E55FFEE77449F948A915FC6B4488B
業務一課:B32CC9BAD4AD4D288CFC7801F50F309D
業務二課:D3862774C18442F2B3959232DE560DA6
代工部:BC79F39918C7442BBC8BD2D20D8F189D
*/

if(this.getProperty("in_sender_role","")!="")
	return this;

if(this.getProperty("classification","")!="受託開發生產評估" && 	
	this.getProperty("classification","")!="新品開發提案評估")
	return this;
	
Innovator inn = this.getInnovator();
string identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList;
string strRole="";
if(identity_list.IndexOf("4E70FC163DF24D3AAC05188C09A27022")>=0)
	strRole += "PM,";
	
if(identity_list.IndexOf("39E66592D4DF4AD780062D9B763A0FB5")>=0)
	strRole += "PD,";

if(identity_list.IndexOf("9D7E55FFEE77449F948A915FC6B4488B")>=0 ||
		identity_list.IndexOf("B32CC9BAD4AD4D288CFC7801F50F309D")>=0 ||
		identity_list.IndexOf("D3862774C18442F2B3959232DE560DA6")>=0)
	strRole += "業務部,";

if(identity_list.IndexOf("BC79F39918C7442BBC8BD2D20D8F189D")>=0)
	strRole += "代工部,";

strRole = strRole.TrimEnd(',');

if(strRole.IndexOf(",")>0)
	throw new Exception("您有多種身分:" + strRole + ",請您自行選擇送件者身分");

if(strRole=="")
	throw new Exception("您不屬於以下身分:PM,PD,業務部,代工部,請您自行選擇送件者身分");
	
this.setProperty("in_sender_role",strRole);
//string sql = "Update In_Project_filing set in_sender_role='" + itmUser.getProperty("in_role","") + "' where id='" + this.getID() + "'";

//inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SetSenderRole' and [Method].is_current='1'">
<config_id>776488A9EB2D486483D8D272574C7D47</config_id>
<name>In_SetSenderRole</name>
<comments>PBF</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
