/*
目的:將本派工單目前的專案任務結束,然後找到下一個專案任務的Leader放到派工單的Owner
位置:In_WorkOrder Workflow 的 「指定新執行者」Auto 節點
作法:
1.找到 in_is_current 的下一個 In_WorkOrder_Detail
2.將原本的 In_WorkOrder_Detail 的 in_is_current 設定為false
3.將新的 In_WorkOrder_Detail 的 in_is_current 設定為 true
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
string strType = this.getType();
string strState = this.getProperty("state");
string sql = "";
string aml = "";

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);


Item itmWFP = _InnH.GetInnoWorkflowProcessByActivity(this);
//1.找到 in_is_current 的下一個 In_WorkOrder_Detail
aml = "<AML>";
aml += "<Item type='In_WorkOrder_Detail' action='get' orderBy='sort_order' select='in_new_assignment,sort_order,in_is_current,related_id(name,managed_by_id,owned_by_id,proj_num)'>";
aml += "<source_id>" + itmWFP.getProperty("in_config_id") + "</source_id>";
aml += "</Item></AML>";

Item itmWorkOrderDetails = inn.applyAML(aml);
int intCurrentWorkOrderDetailIndex = -1;
int intNextWorkOrderDetailIndex = -1;
string strCurSortOrder="";
string strNextSortOrder="";
string strFirstSortOrder = "";

Item itmWorkOrderDetail;

for(int i=0;i<itmWorkOrderDetails.getItemCount();i++)
{
	/*
	1.一旦讀到 in_is_current="1", 代表目前執行中派工任務,要記下strCurSortOrder
	2.如果 本身 detail 的 sort_order 等於 strCurSortOrder,則將in_is_current設為0
	3.如果 strCurSortOrder 有值且 strNextSortOrder無值，則指定strNextSortOrder
	4.如果 本身 detail 的 sort_order 等於 strNextSortOrder,則將in_is_current設為1	
	*/
	
	itmWorkOrderDetail = itmWorkOrderDetails.getItemByIndex(i);
	if(i==0)
		strFirstSortOrder = itmWorkOrderDetail.getProperty("sort_order","0");
	
	//3.如果 strCurSortOrder 有值且 strNextSortOrder無值，則指定strNextSortOrder
	if(strCurSortOrder!=itmWorkOrderDetail.getProperty("sort_order","0") && strCurSortOrder!="" & strNextSortOrder=="")
		strNextSortOrder = itmWorkOrderDetail.getProperty("sort_order","0");

	//1.一旦讀到 in_is_current="1", 代表目前執行中派工任務,要記下strCurSortOrder
	if(itmWorkOrderDetail.getProperty("in_is_current","0")=="1")
	{		
		strCurSortOrder = itmWorkOrderDetail.getProperty("sort_order","0");
	}
	
	if(itmWorkOrderDetail.getProperty("sort_order","0") == strCurSortOrder)
	{
		intCurrentWorkOrderDetailIndex = i;
		itmWorkOrderDetail.setProperty("in_is_current","0");
		itmWorkOrderDetail.apply("edit");
	}		
	
	if(itmWorkOrderDetail.getProperty("sort_order","0") == strNextSortOrder)
	{
		itmWorkOrderDetail.setProperty("in_is_current","1");
		itmWorkOrderDetail.apply("edit");
	}			
}


Item itmWorkOrder;
if(intCurrentWorkOrderDetailIndex==itmWorkOrderDetails.getItemCount()-1)
{
	//代表已經是最後一個了	
	sql = "Update [In_WorkOrder] set ";
	sql += "in_is_detail_complete=1";
	sql += " where id='" + itmWFP.getProperty("in_config_id") + "'";
	inn.applySQL(sql);
}
else
{
	//代表已經是最後一個了	
	sql = "Update [In_WorkOrder] set ";
	sql += "in_is_detail_complete=0";
	sql += " where id='" + itmWFP.getProperty("in_config_id") + "'";
	inn.applySQL(sql);
}

if(intCurrentWorkOrderDetailIndex==-1)
{
	//代表是第一個任務
	intCurrentWorkOrderDetailIndex = 0;		
	intNextWorkOrderDetailIndex = 0;
	sql = "Update [In_WorkOrder_Detail] set ";
	sql += "in_is_current=1";
	sql += " where source_id='" + itmWFP.getProperty("in_config_id") + "' and sort_order=" + strFirstSortOrder;
	inn.applySQL(sql);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;








#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ShiftCurrentWODetail' and [Method].is_current='1'">
<config_id>CC681411A1BB488F82D79FD8535A485A</config_id>
<name>In_ShiftCurrentWODetail</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
