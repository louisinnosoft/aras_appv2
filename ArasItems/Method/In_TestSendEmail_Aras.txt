Innovator inn = this.getInnovator();
string Subject ="EmailTest";
string Message  = "EmailTest";
Item itmEmailMessage = inn.newItem("EMail Message");
itmEmailMessage.setProperty("subject", Subject);
itmEmailMessage.setProperty("body_html", Message);
itmEmailMessage.setProperty("from_user", inn.getUserID());

string aml = "";
aml = "<AML>";
aml += "<Item type='Identity' action='get'>";
aml += "<name condition='in'>'Innovator Admin','Super User','郭毓宗 1235'</name>";
aml += "</Item></AML>";

Item itmIdentities = inn.applyAML(aml);

string strSuccessIdentyNames = "";
string strFailIdentyNames = "";
Item itmFakeDocument = inn.newItem("Document");
for (int i = 0; i < itmIdentities.getItemCount(); i++)
{
    Item itmIdentity = itmIdentities.getItemByIndex(i);
    if (itmFakeDocument.email(itmEmailMessage, itmIdentity))
        strSuccessIdentyNames += itmIdentity.getProperty("name", "") + ",";
    else
        strFailIdentyNames += itmIdentity.getProperty("name", "") + ",";
}
//if(strSuccessIdentyNames!="")
//    Innosoft.InnUtility.AddLog("Email Message發送成功:主旨:" + Subject + ",內容:" + Message + ",對象:" + strSuccessIdentyNames);

//if (strFailIdentyNames != "")
//    Innosoft.InnUtility.AddLog("Email Message發送失敗:主旨:" + Subject + ",內容:" + Message + ",對象:" + strFailIdentyNames);

return itmEmailMessage;
        
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_TestSendEmail_Aras' and [Method].is_current='1'">
<config_id>8E54290C4FBA48609B8EC6AAC007ABB6</config_id>
<name>In_TestSendEmail_Aras</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
