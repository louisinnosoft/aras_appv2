/*
目的:行動方案的打卡功能,支援派工單與設備打卡

//改成相容於新舊版本APP

A.派工單的工時回報-打卡開始功能, 自動為目前的執行人員新增一張工時卡,掛在本派工單關連下,並填入,owner,開始時間
B.設備打卡-打卡開始功能,自動為目前的執行人員新增一張工時卡,掛在本派工單關連下,並填入,owner,開始時間
做法:
1.取得 criteria 資料
2.找到 派工單中，in_is_current任務的in_new_assignment為本人的 in_workorder_detail
3.新增一筆 in_TimeRecord: 
-專案任務=派工單上的專案任務, 
-owner等於自己, 
-in_date_to為空, 
-in_date_from等於現在
-in_workorder_detail = 目前的派工子項
-帶入in_company

2017-08-03
新增工時卡之前先判斷是若有未結束的工時卡,且不是目前派工單,則跳錯

*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string r = "";
string aml = "";
string strMethodName = "In_TimeRecord_Start";
Innosoft.app _InnoApp = new Innosoft.app(inn);
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
_InnH.AddLog(strMethodName,"MethodSteps");
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
string sql = "";
 //要移除 <?xml version="1.0" encoding="utf-8"?>
    if (this.dom.FirstChild.NodeType == XmlNodeType.XmlDeclaration)
    {
      this.dom.RemoveChild( this.dom.FirstChild);
    }
Item itmR = this;
try
{

	string strUserId = inn.getUserID();
	Item itmUser = inn.getItemById("User",strUserId);
	string strCompany = itmUser.getProperty("in_company","");
	string strIdentityId = inn.getUserAliases();


    //string strLoginName = _InnoApp.GetUserInfo(UserInfo, "loginid");
	//Item itmLoginIdentity = _InnH.GetIdentityByUserLoginName(strLoginName);
	//criteria = "itemtype:In_WorkOrder&itemid:attachedId&app:in_web_view";
	
	
	
	string strItemType  = this.getProperty("itemtype","");
	string strItemId = this.getProperty("itemid","");    
	string strProjectId = this.getProperty("projectid","");


	Item itmTimeRecord=null;
	switch(strItemType)
	{
		case "In_WorkOrder":
			aml = "<AML>";
			aml += "<Item type='In_WorkOrder_Detail' action='get'>";
			aml += "<source_id>" + strItemId + "</source_id>";
			aml += "<in_is_current>1</in_is_current>";
			aml += "<in_new_assignment>" + strIdentityId + "</in_new_assignment>";
			aml += "</Item></AML>";

			Item itmWODetails = inn.applyAML(aml);
			if(itmWODetails.isError())
				throw new Exception("本任務不存在或者非執行中狀態");

			Item itmWODetail = itmWODetails.getItemByIndex(0);

			sql = "select id from In_TimeRecord where owned_by_id='" + strIdentityId + "' and in_end_time is null";
			Item itmExistTimerecords = inn.applySQL(sql);
			if(itmExistTimerecords.getResult()!="")
			{
			    //代表不知為何已經存在一筆未結束的打卡紀錄 (正常應該要在client就被擋掉)
			    aml = "<AML>";
			    aml += "<Item type='In_TimeRecord' id='" + itmExistTimerecords.getItemByIndex(0).getID() + "' action='get'>";
			    aml += "</Item></AML>";
			    itmTimeRecord = inn.applyAML(aml);

			    //如果剛好派工單ID又是自己,代表可能是client端連續點擊了兩次,所以就直接跳掉就好否則應該報錯
			    if(itmTimeRecord.getProperty("in_workorder_detail") == itmWODetail.getID())
			    {
			        //do nothing
			        itmTimeRecord = itmTimeRecord;
			    }
			    else
			    {
			        throw new Exception("尚有一筆未完成的任務:[" + itmTimeRecord.getPropertyAttribute("in_workorder_detail","keyed_name","") + "],請先結束後再開始本任務");   
			    }
			}
			else
			{
    			aml = "<AML>";
    			//aml += "<Item type='In_TimeRecord' action='merge' where=\"owned_by_id='" + strIdentityId+ "' and in_workorder_detail='" + itmWODetail.getID() + "' and in_end_time is null\">";
    			aml += "<Item type='In_TimeRecord' action='add'>";
    			aml += "<owned_by_id>" + strIdentityId+ "</owned_by_id>";
    			aml += "<in_start_time>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</in_start_time>";
    			aml += "<in_workorder_detail>" + itmWODetail.getID() + "</in_workorder_detail>";
    			aml += "<in_is_app_created>1</in_is_app_created>";
    			aml += "<in_company>" + strCompany + "</in_company>";
    			aml += "</Item></AML>";
    			itmTimeRecord = inn.applyAML(aml);
			}


			break;
		case "In_Equipment":
			aml = "<AML>";
			aml += "<Item type='In_TimeRecord' action='add'>";
			aml += "<owned_by_id>" + strIdentityId+ "</owned_by_id>";
			aml += "<in_start_time>" + DateTime.Now.ToString("yyyy-MM-ddTHH:mm:ss") + "</in_start_time>";
			aml += "<in_is_app_created>1</in_is_app_created>";
			aml += "<in_company>" + strCompany + "</in_company>";
			aml += "<in_equipment>" + strItemId + "</in_equipment>";
			if(strProjectId!="")
			{
				aml += "<in_project>" + strProjectId + "</in_project>";
			}
			aml += "</Item></AML>";
			itmTimeRecord = inn.applyAML(aml);

			if(strProjectId!="")
			{
				aml = "<AML>";
				aml += "<Item type='In_Project_Equipment' action='merge' where=\"source_id='" + strProjectId + "' and related_id='" + strItemId + "'\">";
				aml += "<source_id>" + strProjectId + "</source_id>";
				aml += "<related_id>" + strItemId + "</related_id>";
				aml += "</Item></AML>";
				Item itmProjEquip = inn.applyAML(aml);				
			}


			break;
		default:
			break;
	}

    Item itmResponse = inn.newItem("response");
	if(itmTimeRecord.isError())
	{
		itmResponse.setProperty("status","error");
		itmResponse.setProperty("message",itmTimeRecord.getErrorString());
	}
	else
	{
		itmResponse.setProperty("status","success");
		itmResponse.setProperty("message","");
	}
	itmR.appendItem(itmResponse);
	
}
catch (Exception ex)
{
  

	string strError = ex.Message + "\n";
    if(strError=="")
	{
		strError = ex.Message + "\n";

		if(ex.Source=="IOM")
		{
			//代表這是非預期的錯誤
			if(aml!="")
				strError += "無法執行AML:" + aml  + "\n";

			if(sql!="")
				strError += "無法執行SQL:" + sql  + "\n";
		}
	}	
    string strErrorDetail="";
	strErrorDetail = strMethodName + ":" + strError; // + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	//strError = strError.Replace("\n","</br>");
	
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return  itmR;
		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_TimeRecord_Start' and [Method].is_current='1'">
<config_id>94C79D47B7894566B28C058FF1455241</config_id>
<name>In_TimeRecord_Start</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
