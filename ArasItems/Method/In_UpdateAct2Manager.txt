/*
目的:將派工單的關聯上的in_new_assignment製換成 Activity2 的 Assignment
做法:
1.找到所有的派工單關聯
3.依據關連上的動做-新增:先判斷是否已存在,若不存在則將in_new_assignment新增至Activity2 的 Assignment
4.依據關連上的動做-移除:將in_new_assignment自Activity2 的 Assignment移除
5.依據關連上的動做-更新任務負責人:將in_new_assignment 取代 managed_by_id
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
string aml = "<AML>";
aml += "<Item type='In_WorkOrder Activity2' action='get'>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";

Item itmWorkOrderAct2s = inn.applyAML(aml);

for(int i=0;i<itmWorkOrderAct2s.getItemCount();i++)
{
	Item itmWorkOrderAct2 = itmWorkOrderAct2s.getItemByIndex(i);
	if(itmWorkOrderAct2.getProperty("related_id")==null)
		continue;
		
	Item itmAct2 = inn.getItemById("Activity2",itmWorkOrderAct2.getProperty("related_id"));
	string strNewAssignment = itmWorkOrderAct2.getProperty("in_new_assignment","");
	string strAction = itmWorkOrderAct2.getProperty("in_action","");
	switch(strAction)
	{
		case "add":
			if(strNewAssignment!=""){
				//3.依據關連上的動做-新增:先判斷是否已存在,若不存在則將in_new_assignment新增至Activity2 的 Assignment
				aml = "<AML>";
				aml += "<Item type='Activity2 Assignment' action='merge' where=\"related_id='" + strNewAssignment + "' and source_id='" + itmAct2.getID() + "'\">";
				aml += "<related_id>" + strNewAssignment + "</related_id>";
				aml += "<source_id>" + itmAct2.getID() + "</source_id>";
				aml += "</Item></AML>";				
				Item itmAct2Ass = inn.applyAML(aml);
				if(itmAct2Ass.isError())
					throw new Exception(itmAct2Ass.getErrorString());
			}
			
			break;
		case "delete":
			if(strNewAssignment!=""){
				//4.依據關連上的動做-移除:將in_new_assignment自Activity2 的 Assignment移除
				aml = "<AML>";
				aml += "<Item type='Activity2 Assignment' action='delete' where=\"related_id='" + strNewAssignment + "' and source_id='" + itmAct2.getID() + "'\">";
				aml += "</Item></AML>";				
				Item itmAct2Ass = inn.applyAML(aml);
				if(itmAct2Ass.isError())
					throw new Exception(itmAct2Ass.getErrorString());
			}
			break;
		case "none":
			break;
		case "change_leader":
			itmAct2.setPropertyAttribute("lead_role","is_null","1");
			itmAct2.setProperty("managed_by_id",itmWorkOrderAct2.getProperty("in_new_assignment"));
			itmAct2 = itmAct2.apply("edit");	
			break;			
		default:
			break;
	}
	
	
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateAct2Manager' and [Method].is_current='1'">
<config_id>2559D320B1DA4244B265A531D3393673</config_id>
<name>In_UpdateAct2Manager</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
