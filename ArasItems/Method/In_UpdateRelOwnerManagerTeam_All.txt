Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string errmsg = "";
string okmsg = "";
int errcount=0;
int okcount=0;
int totalcount=0;
string strTypes = "In_PerlWorkRecord";//in_dayreport,In_Employee_Payment,In_Invoice,in_proposal,In_Timesheet_Auditing,In_Vendor_Payment,in_contract,In_TimeRecord
string strMethod = "In_UpdateRelOwnerManagerTeam";

string[] arrTypes = strTypes.Split(',');

for(int k=0;k<arrTypes.Length;k++)
{
    totalcount++;
    aml = "<AML>";
    aml += "<Item type='" + arrTypes[k] + "' action='get'>";
    aml += "</Item></AML>";

    Item itmItems = inn.applyAML(aml);
    for(int i=0;i<itmItems.getItemCount();i++)
    {
    	Item itmItem =  itmItems.getItemByIndex(i);
    	try{
    	    itmItem.apply(strMethod);
    	}
    	catch(Exception ex)
    	{
    	    errmsg += "[" + arrTypes[k] + "]-" + itmItem.getProperty("keyed_name") + ":" + ex.Message + "<br>";
    	    errcount ++;
    	    Innosoft.InnUtility.AddLog(errcount.ToString(),"GetProjectInfo_All");
    	    continue;
    	}
    	okcount++;

    }

}

Item itmR = this;
string strMessage = "";
strMessage = "總共:" + totalcount.ToString() + "\n";
strMessage += "成功:" + okcount.ToString() + "\n";
strMessage += "失敗:" + errcount.ToString() + "\n";
strMessage += "失敗訊息:" + errmsg;
itmR = inn.newResult(strMessage);

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateRelOwnerManagerTeam_All' and [Method].is_current='1'">
<config_id>1368E5CBA13240D5974700BBA31A4FFC</config_id>
<name>In_UpdateRelOwnerManagerTeam_All</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
