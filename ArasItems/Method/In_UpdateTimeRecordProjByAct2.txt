/*
目的:取得任務的專案編號
*/

Innovator inn = this.getInnovator();

if(this.getProperty("in_activity2","")=="")
	return this;

Item itmAct2 = inn.getItemById("Activity2",this.getProperty("in_activity2",""));

string strProjId = itmAct2.getProperty("in_proj_id","");
if(itmAct2.getProperty("in_proj_id","")!="")
	this.setProperty("in_project",strProjId);

return this;	
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateTimeRecordProjByAct2' and [Method].is_current='1'">
<config_id>097ADE7BBA4F4D33966898704936FD45</config_id>
<name>In_UpdateTimeRecordProjByAct2</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
