/*
撰寫:JOE
目的:計算欄位中的數值
做法:
1.使用SumRelationshipPropertyToSourceProperty
位置:onAfterAdd,onAfterUpdate
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string aml = "";
string sql = "";
int intToTal = 0;

aml = "<AML>";
aml += "<Item type='In_Vendor_Payment_detail_2' action='get'>";
aml += "<source_id>"+this.getID()+"</source_id>";
aml += "</Item></AML>";
Item itmVendorPaymentDetails = inn.applyAML(aml);
for(int i=0;i<itmVendorPaymentDetails.getItemCount();i++){
	Item itmVendorPaymentDetail = itmVendorPaymentDetails.getItemByIndex(i);
	
	Item itmContractDetail = inn.getItemById("in_contract_detail",itmVendorPaymentDetail.getProperty("related_id",""));
	
	int intMoneyTax = Convert.ToInt32(itmContractDetail.getProperty("in_money_tax",""));
	
	intToTal += intMoneyTax;
}
sql = "Update In_Vendor_Payment set ";
sql += "in_payment_o = '" + intToTal + "'";
sql += " where id = '" + this.getID() + "'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Vendor_Payment_Total' and [Method].is_current='1'">
<config_id>270474E310614653BC0C3583B646A5AF</config_id>
<name>In_Vendor_Payment_Total</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
