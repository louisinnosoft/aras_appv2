/*
目的:判斷廠商類別
做法:
1.找出廠商
2.若是法人而無欄位則跳錯

onAfterUpdate,onAfterAdd
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);
    
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";

try
{

aml = "<AML>";
aml += "<Item type='Vendor' action='get'>";
aml += "<id>"+this.getProperty("in_vendor","")+"</id>";
aml += "</Item></AML>";
Item itmVendor = inn.applyAML(aml);

if(itmVendor.getProperty("in_type","") == "2"){
    if(this.getProperty("in_invoice_number","") == ""){
        return inn.newError("[廠商分類]為法人,[發票號碼]不得為空白");
    }
}

}
catch(Exception ex)
{
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
    
    string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
    
    strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
    strMethodName = strMethodName.Replace("EventArgs)","");
    
    string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
    string strError = ex.Message + "\n";
    
    if(aml!="")
        strError += "無法執行AML:" + aml  + "\n";
        
    if(sql!="")
        strError += "無法執行SQL:" + sql  + "\n";
        
    string strErrorDetail="";
    
    strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
    Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
    throw new Exception(_InnH.Translate(strError));
}
if(PermissionWasSet)
Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Vendor_classification' and [Method].is_current='1'">
<config_id>2D05A2EDD97B4A97A7D07328970F8DFA</config_id>
<name>In_Vendor_classification</name>
<comments>判斷廠商類別</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
