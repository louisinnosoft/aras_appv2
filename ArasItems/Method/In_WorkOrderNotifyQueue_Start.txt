/*
目的:當派工單流程進行到工作執行中時,會發出推播訊息通知當下的執行者
*/
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Item ControlledItem = _InnH.GetInnoControlledItem(this);

string aml = "";
string queue = "";

Item itmVariable = inn.getItemByKeyedName("In_Variable","app_url");
string strAppUrl = itmVariable.getProperty("in_value","");
string strIcoUrl = strAppUrl.Replace("a/a.aspx","images/home/logo.ico");

aml = "<AML>";
aml += "<Item type='Activity Assignment' action='get'>";
aml += "<source_id>" + this.getID() + "</source_id>";
aml += "</Item></AML>";
Item itmActs = inn.applyAML(aml);

for(int i=0;i<itmActs.getItemCount();i++){
	Item itmAct = itmActs.getItemByIndex(i);

	string strToUser = itmAct.getProperty("related_id","");
	string strMsg = "";
	string strURL = "?redir=Controls/ActVote.htm?actassid=" + itmAct.getID();
	string strSourceType = "In_WorkOrder";
	string strConfigID = ControlledItem.getProperty("config_id","");


	aml = "<AML>";
	aml += "<Item type='In_Workorder_Detail' action='get' select='in_name'>";
	aml += "<source_id>" + ControlledItem.getID() + "</source_id>";
	aml += "<in_is_current>1</in_is_current>";
	aml += "<in_new_assignment>" + strToUser + "</in_new_assignment>";
	aml += "</Item></AML>";

	Item itmWorkOrderDetail = inn.applyAML(aml);
	strMsg = itmWorkOrderDetail.getProperty("in_name","");


	//queue = "<to_user>" + strToUser + "</to_user>";
	queue = "<title>派工單任務</title>";
	queue += "<message>" + strMsg + "</message>";
	queue += "<url>" + strURL +"</url>";
	queue += "<icourl>" + strIcoUrl + "</icourl>";
	queue += "<wodetail_id>" + itmWorkOrderDetail.getID() + "</wodetail_id>";

	//queue += "<source_type>" + strSourceType + "</source_type>";
	//queue += "<source_cfgid>" +strConfigID + "</source_cfgid>";

	Item itmQueue = _InnH.CreateQueue(this,"notify-read",queue,"");
	string sql = "Update [In_Queue] set owned_by_id='" + strToUser + "' where id='" + itmQueue.getID() + "'";
	inn.applySQL(sql);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_WorkOrderNotifyQueue_Start' and [Method].is_current='1'">
<config_id>B2DAA764D29247CB94F1F62252378E48</config_id>
<name>In_WorkOrderNotifyQueue_Start</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
