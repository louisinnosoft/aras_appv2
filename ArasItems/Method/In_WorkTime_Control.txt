string strMethodName = "In_WorkTime_Control";

/*
目的:檢查工時卡內容是否符合規定
做法:
0.檢查是否為員工
1.TimeSheetManager與HR不受限制
2.專案狀態必須為Active且不能為99999
3.檢查打卡日
4.檢查報工功能代碼,若沒有報工代碼則忽略本檢查
5.假日不得打卡
6.檢查是否超時
7.寫入打卡者的公司別與總工時
8.顯示錯誤訊息
位置:
onAfterAdd,onAfterUpdate
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
string strErrInfo = "<br/>";

Item itmR = this;
Item itmSQL = null;
Item itmProject = null;

try
{
    double dblTotalHour = 0;
	string strMemberID = "";
	string strDescriptions = "";
	string strHoliday = "";
	string strTimeRecordIDs = "";

	//取得現在的日期時間
	DateTime dtNowDateTime = DateTime.Now;
	string strNowDateTime = dtNowDateTime.ToString("yyyy-MM-ddTHH:mm:ss");

	//取得現在的年月日時間
	string strNow = strNowDateTime.Replace("T","-");
	string[] strNowDetail = strNow.Split('-');

	//取得工作日
	Item itmApplyMethod = inn.applyMethod("In_GetWorkDays","<BasisDay>"+strNowDateTime+"</BasisDay><Days>-3</Days><Interval>0</Interval>");
	string strWorkDays = itmApplyMethod.getProperty("workdays","");
	string[] strWorkDay = strWorkDays.Split(',');

	//取得TimeSheetManager
	aml = "<AML>";
	aml += "<Item type='Identity' action='get'>";
	aml += "<name>Timesheet Manager</name>";
	aml += "</Item></AML>";
	Item itmTimesheetManager = inn.applyAML(aml);
	string strTimeSheetManager = itmTimesheetManager.getID();

	//取得TimeSheetManager的成員
	aml = "<AML>";
	aml += "<Item type='Member' action='get'>";
	aml += "<source_id>" + itmTimesheetManager.getID() + "</source_id>";
	aml += "</Item></AML>";
	Item itmMembers = inn.applyAML(aml);
	for(int i=0;i<itmMembers.getItemCount();i++){
		Item itmMember = itmMembers.getItemByIndex(i);
		strMemberID += itmMember.getProperty("related_id","") + ",";
	}
	strMemberID = strMemberID.Trim(',');

	//取得所有登入者
	string strLogIns = Aras.Server.Security.Permissions.Current.IdentitiesList;

	//取得打卡者的工時
	Item itmUser = _InnH.GetUserByAliasIdentityId(this.getProperty("owned_by_id"));
	int intDayHour = Convert.ToInt32(itmUser.getProperty("in_day_hour",""));

	//取得打卡者的工時紀錄表
	aml = "<AML>";
	aml += "<Item type='In_TimeRecord' action='get'>";
	aml += "<owned_by_id>" + this.getProperty("owned_by_id","") + "</owned_by_id>";
	aml += "<NOT><in_type condition='like'>5*</in_type></NOT>";
	aml += "</Item></AML>";
	Item itmTimeRecords = inn.applyAML(aml);

	//取得專案
	if(this.getProperty("in_project","") !="")
	{
	    itmProject = inn.getItemById("Project",this.getProperty("in_project",""));
	}

	//取得打卡日
	string strStartTime = this.getProperty("in_start_time","");
	strStartTime = strStartTime.Split('T')[0];

	//計算打卡者同日的總工時並取得同日的工時紀錄表
	for(int i=0;i<itmTimeRecords.getItemCount();i++){
		Item itmTimeRecord = itmTimeRecords.getItemByIndex(i);
		string strTime = itmTimeRecord.getProperty("in_start_time","");
		strTime = strTime.Split('T')[0];

		if(strStartTime == strTime){
			double dblWorkHour = Convert.ToDouble(itmTimeRecord.getProperty("in_work_hours","0"));
			dblTotalHour += dblWorkHour;
			strTimeRecordIDs += itmTimeRecord.getID() + ",";
		}
	}
	strTimeRecordIDs = strTimeRecordIDs.Trim(',');
	strTimeRecordIDs = strTimeRecordIDs.Replace(",","','");

	//取得今年的行事曆
	aml = "<AML>";
	aml += "<Item type='Business Calendar Year' action='get'>";
	aml += "<year>" + strNowDetail[0] + "</year>";
	aml += "</Item></AML>";
	Item itmYear = inn.applyAML(aml);



	//必填檢查
	if(this.getProperty("in_is_import","")=="1"||this.getProperty("in_is_hr","")=="1"){
	    if(this.getProperty("in_company","")==""){
	        strErrInfo += "[公司別]為必填欄位,";
	    }
	    if(this.getProperty("in_start_time","")==""){
	        strErrInfo += "[報工日期]為必填欄位,";
	    }
	    if(this.getProperty("in_work_hours","")==""){
	        strErrInfo += "[工時]為必填欄位,";
	    }
	    if(this.getProperty("owned_by_id","")==""){
	        strErrInfo += "[員工]為必填欄位,";
	    }
	    if(this.getProperty("in_function_code","")==""){
	        strErrInfo += "[報工功能代碼]為必填欄位,";
	    }
	    //if(this.getProperty("in_project","")==""){
	        //strErrInfo += "[專案名稱]為必填欄位,";
	    //}
	    if(this.getProperty("description","")==""){
	        strErrInfo += "[工作說明]為必填欄位,";
	    }
	    if(this.getProperty("description","")==""){
	        strErrInfo += "[工作說明]為必填欄位,";
	    }
	}
	strErrInfo = strErrInfo.Trim(',');

	//0.檢查是否為員工
	if(itmUser.getProperty("logon_enabled","")!="1"){
	    strErrInfo += itmUser.getProperty("keyed_name","")+"非本公司員工<br/>";
	}

	//普通匯入檢查
	if(this.getProperty("in_is_import","")=="1"){
	    //2.專案狀態必須為Active且不能為99999

	    if(itmProject != null)
	    {
	        if(itmProject.getProperty("state","") != "Active"){
		    //throw new Exception("專案狀態必須為Active");
		    strErrInfo += "專案狀態必須為Active<br/>";
	        }
	        if(itmProject.getProperty("in_number","") == "99999"){
		    strErrInfo += "專案名稱不得為99999<br/>";
	        }
	    }

		//3.檢查打卡日
		//一般版本不需要檢查打卡日
		if(!strWorkDays.Contains(strStartTime)){			
			strErrInfo += "打卡日必須為:" + strWorkDay[0] + "," + strWorkDay[1] + "," + strWorkDay[2] + "," + strWorkDay[3] + ";";
		}
		
		//4.檢查報工功能代碼
		string strFunctionCode = "L00,L01,L02,L03,L04,L05,L06,L07,L08,L09";

		aml = "<AML>";
	    aml += "<Item type='In_Function_Code' action='get'>";
	    aml += "<id>"+this.getProperty("in_function_code","")+"</id>";
	    aml += "</Item></AML>";
	    Item itmFunctionCode = inn.applyAML(aml);

	    if(itmFunctionCode.getProperty("state","")!="released" || strFunctionCode.Contains(itmFunctionCode.getProperty("in_code",""))){
	        strErrInfo += itmFunctionCode.getProperty("in_code","") + "為非正確的報工功能代碼<br/>";
	    }
	}

	//1.TimeSheetManager與HR不受限制
	if(strMemberID.Contains(this.getProperty("owned_by_id","")) || strLogIns.Contains(strTimeSheetManager) || this.getProperty("in_is_hr","")=="1"){
	}else{
	    //2.專案狀態必須為Active且不能為99999
	    if(itmProject != null)
	    {
	        if(itmProject.getProperty("state","") != "Active"){
		    //throw new Exception("專案狀態必須為Active");
		    strErrInfo += "專案狀態必須為Active<br/>";
		    }
		    if(itmProject.getProperty("in_number","") == "99999"){
		        strErrInfo += "專案名稱不得為99999<br/>";
		    }
	    }

	    //3.檢查打卡日
		if(!strWorkDays.Contains(strStartTime)){
			//throw new Exception("打卡日必須為:<br/>" + strWorkDay[0] + "<br/>" + strWorkDay[1] + "<br/>" + strWorkDay[2] + "<br/>" + strWorkDay[3]);
			strErrInfo += "打卡日必須為:" + strWorkDay[0] + "," + strWorkDay[1] + "," + strWorkDay[2] + "," + strWorkDay[3] + ";";
		}

		//4.檢查報工功能代碼
		if(this.getProperty("in_function_code","")!="")
		{
			string strFunctionCode = "L00,L01,L02,L03,L04,L05,L06,L07,L08,L09";

			aml = "<AML>";
			aml += "<Item type='In_Function_Code' action='get'>";
			aml += "<id>"+this.getProperty("in_function_code","")+"</id>";
			aml += "</Item></AML>";
			Item itmFunctionCode = inn.applyAML(aml);
			if(itmFunctionCode.isError())
			{
				strErrInfo += "系統內查無:[" + itmFunctionCode.getProperty("in_code","") + "]報工功能代碼<br/>";
			}
			else
			{
				if(itmFunctionCode.getProperty("state","")!="released" || strFunctionCode.Contains(itmFunctionCode.getProperty("in_code",""))){
					strErrInfo += itmFunctionCode.getProperty("in_code","") + "為非正確的報工功能代碼<br/>";
				}
			}




		}

	    //檢查工時(不得為負數)
	    double dblWorkHour = Convert.ToDouble(this.getProperty("in_work_hours"));
	    if(dblWorkHour<0){
	        strErrInfo += "工時不得為負數<br/>";
	    }

	    //5.假日不得打卡
		strHoliday = "周休二日";
		Item itmIsHoliday = inn.applyMethod("In_GetWorkDays","<BasisDay>"+strNowDateTime+"</BasisDay><Days>0</Days><Interval>0</Interval>");		
		string strIsHoliday = itmIsHoliday.getProperty("workdays","");
		if(strIsHoliday.Split('T')[0]!=strNowDateTime.Split('T')[0])
		{
			//代表今天是假日
			aml = "<AML>";
			aml += "<Item type='Business Calendar Exception' action='get'>";
			aml += "<source_id>" + itmYear.getID() + "</source_id>";
			aml += "<day_date>" + dtNowDateTime.ToString("yyyy-MM-dd") + "T00:00:00</day_date>";
			aml += "<day_off>1</day_off>";
			aml += "</Item></AML>";
			Item itmHoliday = inn.applyAML(aml);
			if(!itmHoliday.isError())
			{
				//代表是國定假日
				strHoliday = itmHoliday.getProperty("description","");
			}
			strErrInfo += "休假日:" + dtNowDateTime.ToString("yyyy-MM-dd") + ":" + strHoliday + ",不得打卡<br/>";
		}


	    //6.檢查是否超時
	    if(dblTotalHour > intDayHour){
	        //throw new Exception(itmUser.getProperty("first_name","") + " 一天工時為 " + intDayHour + " 小時,目前已超過總工時 "+ dblTotalHour);
	        strErrInfo += itmUser.getProperty("first_name","") + " 一天工時為 " + intDayHour + " 小時,目前已超過總工時 "+ dblTotalHour;
	    }
	}

	//8.顯示錯誤訊息
	if(strErrInfo != "<br/>"){
	    if(this.getProperty("in_is_hr","")=="1"){
	        sql = "Update [In_TimeRecord] set [in_importmsg]='" + strErrInfo + "' where id in ('" + itmR.getID() + "')";
	        itmSQL = inn.applySQL(sql);
	    }else{
	        throw new Exception(strErrInfo);
	    }
	}

	//7.寫入打卡者的公司別與總工時
	sql = "Update [In_TimeRecord] set [in_today_hours]='" + dblTotalHour + "' where id in ('" + strTimeRecordIDs + "')";
	itmSQL = inn.applySQL(sql);
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(strError);

	strError = ex.Message + "\n";

// 	if(this.getProperty("in_is_hr","")=="1")
// 	{
// 		strError = "匯入失敗:[" + this.getPropertyAttribute("owned_by_id","keyed_name") + "]的工時卡[" + this.getProperty("item_number","") + "],發生以下錯誤:" + ex.Message;
// 		this.apply("delete");
// 	}

// 	if(this.getProperty("in_is_hr","")=="0")
// 	{
// 		strError = "匯入失敗:[" + this.getPropertyAttribute("owned_by_id","keyed_name") + "]的工時卡[" + this.getProperty("item_number","") + "],發生以下錯誤:" + ex.Message;
// 		this.apply("delete");
// 	}
//Joe_20170918_行動版跳出錯誤訊息則解除鎖定
if(itmR.getProperty("in_is_app_created") == "1")
{
    if(itmR.getLockStatus() != 0)
    {
        itmR.unlockItem();
    }
}

	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_WorkTime_Control' and [Method].is_current='1'">
<config_id>92F89B74CE7146F18B82F0B41A8E4448</config_id>
<name>In_WorkTime_Control</name>
<comments>檢查工時卡內容是否符合規定</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
