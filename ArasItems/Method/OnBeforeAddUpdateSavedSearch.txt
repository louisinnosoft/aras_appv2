if ("Main Grid" != this.getProperty("location"))
  this.setProperty("show_on_toc", "0");
var savedSearchCriteria = this.getProperty("criteria");
if (!string.IsNullOrEmpty(savedSearchCriteria))
{
  var savedSearchCriteriaItem = this.newItem();
  savedSearchCriteriaItem.loadAML(savedSearchCriteria);
  savedSearchCriteriaItem.removeAttribute("order_by");

  this.setProperty("criteria", savedSearchCriteriaItem.ToString());
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnBeforeAddUpdateSavedSearch' and [Method].is_current='1'">
<config_id>B959C0434FAB41F1ADAFEF58A2B500BE</config_id>
<name>OnBeforeAddUpdateSavedSearch</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
