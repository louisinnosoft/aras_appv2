/*version:5*/
var _isVers = (aras.getItemProperty(document.item, 'is_versionable') == '1');
var _isRel = (aras.getItemProperty(document.item, 'is_relationship') == '1');

var versioningDisciplineFld = document.forms.MainDataForm.elements['versioning_discipline'];
var _usaChb = document.forms.MainDataForm.elements['use_src_access'];

var expressionFunctionCode = '' +
	'var _mv_chb = document.getElementById(\'' + versioningDisciplineFld.id + '\'); \r\n ' +
	'if(!isEditMode) \r\n' +
	'{\r\n' +
		'_mv_chb.disabled = true;\r\n' +
		'_mv_chb.parentNode.className = "sys_f_div_select sys_f_div_select_disabled";\r\n' +
		'return;\r\n' +
	'} else {\r\n' +
		'_mv_chb.parentNode.className = "sys_f_div_select";\r\n' +
	'}\r\n' +
	'var _isVers = (aras.getItemProperty(document.item, \'is_versionable\') == \'1\');\r\n' +
	'if (_mv_chb)  _mv_chb.disabled  = (! _isVers);\r\n';

var evalMethod = window.eval;
evalMethod('expression_' + versioningDisciplineFld.id + '_setExpression = function(isEditMode){' + expressionFunctionCode + '}');

var expressionFunctionCode = '' +
	'var _usa_chb = document.getElementById(\'' + _usaChb.id + '\'); \r\n ' +
	'if(!isEditMode) \r\n' +
	'{\r\n' +
		'_usa_chb.disabled = true;\r\n' +
		'return;\r\n' +
	'}\r\n' +
	'var _isRel  = (aras.getItemProperty(document.item, \'is_relationship\') == \'1\');' +
	'if (_usa_chb)  _usa_chb.disabled  = (! _isRel);\r\n';

evalMethod('expression_' + _usaChb.id + '_setExpression = function(isEditMode){' + expressionFunctionCode + '}');

//  if (_mv_chb)  _mv_chb.disabled  = (! _isVers);
//  if (_usa_chb) _usa_chb.disabled = (! _isRel);

evalMethod('expression_' + versioningDisciplineFld.id + '_setExpression(document.isEditMode);');
evalMethod('expression_' + _usaChb.id + '_setExpression(document.isEditMode);');

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='OnLoad ItemType' and [Method].is_current='1'">
<config_id>176A7CA23A034D86A2E995859F9BAB5B</config_id>
<name>OnLoad ItemType</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
