if (propertyName=="fake_verified")
{
  if (newValue=="1")
    item.setProperty("percent_compl", "100");
  if (newValue!="1" && item.getProperty("percent_compl")=="100")
    item.setProperty("percent_compl", "99");
}
else if (propertyName=="percent_compl")
{
  var v = (newValue=="100") ? "1" : "0";
  item.setProperty("fake_verified", v);
}
grid.invalidateContent();
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_ACW_cfgTasksGridSyncPCandVrfd' and [Method].is_current='1'">
<config_id>4C5B46161F4341499CBA468CA3273644</config_id>
<name>PM_ACW_cfgTasksGridSyncPCandVrfd</name>
<comments>Synchronizes percent_compl and is_verified onEditFinish in Tasks grid of ACW.</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
