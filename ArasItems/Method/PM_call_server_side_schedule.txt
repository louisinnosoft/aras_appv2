var schedCanNotBeCalled = inDom.selectSingleNode("descendant-or-self::Item[@isDirty='1' or (string(@action)!='' and string(@action)!='get')]");
if (schedCanNotBeCalled)
  return top.aras.AlertError(top.aras.getResource("project", "pr_methods.scheduling_cannot_for_unsaved_item"));

var q = new Item("Method", "Schedule Project");
var id = inDom.getAttribute("id");
q.setProperty("body", "");
q.setPropertyAttribute("body", "project_id", id);
var r = q.apply();
if (r.isError())
  return top.aras.AlertError(r);

//Reload project
top.aras.removeFromCache(id);
var w = top.aras.uiFindWindowEx(id);
if (w)
{
  top.aras.getItemById("Project", id, 0);
  top.aras.uiReShowItem(id, id, "tab view");
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_call_server_side_schedule' and [Method].is_current='1'">
<config_id>A0442DC19F6A4833965F1B6B4EDF7FDD</config_id>
<name>PM_call_server_side_schedule</name>
<comments>Calls server side schedule method</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
