var toolbarFrm = this.document.getElementById('toolbar_slot_custom_iframe');
if (!toolbarFrm) return;
toolbarFrm = toolbarFrm.contentWindow;
var selectedId = gridApplet.getSelectedId();
if (selectedId && item)
{
  var tmpItm = item.selectSingleNode("//Item[@id='" + selectedId + "' and not(@action = 'delete')]");
  if (!tmpItm) selectedId = "";
}
toolbarFrm.setControlEnabled("del", selectedId);
toolbarFrm.setControlEnabled("view", selectedId);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_handleTbarDisablingInACW' and [Method].is_current='1'">
<config_id>EA731245E5BF4EB8A14ED4CA3524CCE5</config_id>
<name>PM_handleTbarDisablingInACW</name>
<comments>Handles buttons enabling/disabling in some grids of ACW</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
