var errorMsg = "";

if (top.aras.isTempEx(this.node) || top.aras.isDirtyEx(this.node) || top.aras.isNew(this.node)) {
	errorMsg = top.aras.getResource("Project", "project.cannot_be_cloned", "Project");
	top.aras.AlertError(errorMsg);
	return;
}

var clone = this.newItem();
clone.loadAML("<AML>" + this.node.xml + "</AML>");
clone.setAttribute("projectCloneMode", "CreateProjectFromProject");
var newProject = clone.apply("Project_CloneProjectOrTemplate");
if (newProject.isError()) {
	 top.aras.AlertError(newProject.getErrorString(), newProject.getErrorDetail());
}
top.aras.uiShowItemEx(newProject.node);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Project_CreateProjectFromProject' and [Method].is_current='1'">
<config_id>F939E0E6B5A649559B54B1FDCD97CB9C</config_id>
<name>Project_CreateProjectFromProject</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
