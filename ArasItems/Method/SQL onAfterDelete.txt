' name: SQL onAfterDelete
' created: 5-OCT-2005 George J. Carrette
'MethodTemplateName=VBScriptMain;

Sub Main(inDom As XmlDocument,outDom As XmlDocument,Iob as Object,Svr As Object,Ses As Object)
 cco.xml.setitemproperty(cco.xml.GetRequestItem(indom),"PROCESS","onAfterDelete")
 cco.applyitem.ExecuteMethodByName(indom,outdom,"SQL PROCESS",True)
End Sub


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='SQL onAfterDelete' and [Method].is_current='1'">
<config_id>6994B2B10F7D4B379EA1D0152C0599A8</config_id>
<name>SQL onAfterDelete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
