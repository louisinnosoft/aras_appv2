string type = this.getProperty("forum_type", "");
string owner = this.getProperty("owned_by_id", "");
if (!String.IsNullOrEmpty(owner) && !String.IsNullOrEmpty(type) && type == "MyBookmarks")
{
	if (!CanCreateMyBookmarksForum(owner))
	{
		throw new Exception(CCO.ErrorLookup.Lookup("SSVC_UserCannotHaveMoreThanOneMyBookmarks"));
	}
}
return this;
}

internal bool CanCreateMyBookmarksForum(string owner)
{
	Item item = this.newItem("Forum", "get");
	item.setProperty("owned_by_id", owner);
	item.setProperty("forum_type", "MyBookmarks");
	item = item.apply();
	if (item.isError() && item.getItemCount() < 1)
	{
		return true;
	}
	else return false;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_AddForumValidation' and [Method].is_current='1'">
<config_id>4E530BB368444C8E9FB4A31B802A65A6</config_id>
<name>VC_AddForumValidation</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
