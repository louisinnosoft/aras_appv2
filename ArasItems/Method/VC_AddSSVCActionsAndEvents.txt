	inn = this.getInnovator();
	var itemTypeId = this.getProperty("source_id");
	AddCreateDiscussionDefinitionActionIfNeed(itemTypeId);
	return this;
}

private Innovator inn;

private void AddCreateDiscussionDefinitionActionIfNeed(String itemTypeId)
{
	const string actionId = "5B155B40AD8F405D812EF95BC5848214"; // action "Create Discussion Definition"
	Item itemAction = inn.newItem("Item Action", "get");
	itemAction.setProperty("source_id", itemTypeId);
	itemAction.setProperty("related_id", actionId);
	itemAction = itemAction.apply();
	if (itemAction.isError())
	{
		itemAction = this.newItem("Item Action", "add");
		itemAction.setAttribute("doGetItem", "0");
		itemAction.setProperty("source_id", itemTypeId);
		itemAction.setProperty("related_id", actionId);
		itemAction = itemAction.apply();
		if (itemAction.isError())
		{
			throw new Exception(itemAction.getErrorString());
		}
	}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_AddSSVCActionsAndEvents' and [Method].is_current='1'">
<config_id>D25FCCFCF6254510BE9502941782B82F</config_id>
<name>VC_AddSSVCActionsAndEvents</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
