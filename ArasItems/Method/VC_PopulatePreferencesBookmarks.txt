var selectElement = document.getElementById('3C7AA413976347CCAFCF2B4F90A241FB');
var identityId = getPreferenceIdentityId();
var user = getUserByIdentityId(identityId);

if (user.isError() && user.getErrorCode() !== '0') {//if error == 0 then user is not found (identity without alias)
	aras.AlertError(user.getErrorString());
	return;
}
var userId = user.isError() ? '' : user.getAttribute('id'); //if identity without alias - user id is empty
var forumsOptionsString = getForumsOptions(userId);
var currentBookmark = aras.getItemProperty(document.item, 'default_bookmark');
//populating select and attaching change handling
setupSelect(selectElement, forumsOptionsString, currentBookmark);

/*
	Functions:
*/

function getPreferenceIdentityId() {
	return aras.getItemProperty(window.parent.parent.getItem(), 'identity_id');
}

function getUserByIdentityId(id) {
	var userItem = aras.newIOMItem('User', 'get');
	userItem.setAttribute('select', 'id');
	var aliasItem = aras.newIOMItem('Alias', 'get');
	aliasItem.setAttribute('select', 'id');
	aliasItem.setProperty('related_id', id);
	userItem.addRelationship(aliasItem);

	return userItem.apply();
}

function getForumsForUser(userId) {
	var forums = aras.newIOMItem('User', 'VC_GetForumsForUser');
	forums.setAttribute('id', userId);
	return forums.apply();
}

function getForumsOptions(userId) {
	var forumsOptionsString;
	var forumsItem;

	if (userId) {
		forumsItem = getForumsForUser(userId);
		if (forumsItem.isError()) {
			if (forumsItem.getErrorCode() !== 0) {
				throw new Error(forumsItem.getErrorString());
			} else {
				forumsItem = aras.newIOMItem('Forum');
				forumsItem.setProperty('forum_type', 'MyBookmarks');
			}
		} else {
			var myBookmarkForum = forumsItem.getItemsByXPath('//Item[@type=\'Forum\'][forum_type=\'MyBookmarks\']');
			myBookmarkForum.setAttribute('currentUser', userId);
		}
	} else {
		forumsItem = aras.newIOMItem('Forum');
		forumsItem.setProperty('forum_type', 'MyBookmarks');
	}

	return aras.applyXsltFile(forumsItem.dom, aras.getScriptsURL() + '../Modules/aras.innovator.SSVC/styles/bookmarksPreferences.xslt');
}

function setupSelect(selectElement, forumsOptionsString, defaultId) {
	var options = selectElement.options;
	var selectedIndex = 0;
	var selectedText = '';
	var forumsDocument = aras.createXMLDocument();

	//Parsing options with xml document cause you just cannot add them
	//directly to dom without creation of option elements.
	forumsDocument.loadXML(forumsOptionsString);
	var optionsNodes = forumsDocument.documentElement.childNodes;

	for (var i = 0, count = optionsNodes.length; i < count; i++) {
		option = document.createElement('option');
		option.value = optionsNodes[i].getAttribute('value');
		option.text = optionsNodes[i].text;
		selectElement.appendChild(option);

		if (option.value == defaultId) {
			selectedIndex = i;
			selectedText = option.text;
		}
	}

	selectElement.selectedIndex = selectedIndex;
	var span = document.getElementById('selected_option_3C7AA413976347CCAFCF2B4F90A241FB');
	span.textContent = selectedText;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_PopulatePreferencesBookmarks' and [Method].is_current='1'">
<config_id>F32C2178B8D847ABB5766A156383632D</config_id>
<name>VC_PopulatePreferencesBookmarks</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
