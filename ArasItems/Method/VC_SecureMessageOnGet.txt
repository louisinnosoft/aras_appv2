//MethodTemplateName=CSharpInOut;
string [] smRelationships = {
	"SecureMessageMarkup",
	"SecureMessageAudio",
	"SecureMessageVideo"
};
var connection = (Aras.Server.Core.IOMConnection) InnovatorServerASP;
Aras.Server.Core.CallContext CCO = connection.CCO;
Innovator inn = new Innovator(connection);
Item itm = inn.newItem();
itm.loadAML(inDom.OuterXml);
string type = itm.getType();
if (itm.getAttribute("serverEvents") == "0")
{
	throw new Aras.Server.Core.InnovatorServerException(string.Format(CCO.ErrorLookup.Lookup("SSVC_ItemsCantBeObtained"), type));
}
Aras.Server.Security.Identity identity = Aras.Server.Security.Identity.GetByName("SecureMessageReaderWriter");
bool permsWasSet = Aras.Server.Security.Permissions.GrantIdentity(identity);
Item resultItem = inn.newItem();
try
{
	eventData.BaseGetItem(outDom);
	resultItem.loadAML(outDom.InnerXml);
	if (resultItem.isError())
	{
		if (resultItem.getErrorCode() == "0" && smRelationships.Contains(type))
		{
			outDom.LoadXml(inn.newResult("").dom.InnerXml);
		}
	}
	else
	{
		XmlNodeList secureMessages = outDom.SelectNodes(Item.XPathResultItem);
	 	for (int i = secureMessages.Count - 1; i >= 0; i--)
		{
			XmlElement secureMessage = secureMessages[i] as XmlElement;
			if (!String.IsNullOrEmpty(connection.CCO.XML.GetItemProperty(secureMessage, "disabled_by_id")))
			{
				Aras.Server.Core.XML.SetItemPropertyNullInternal(secureMessage, "comments", true);
				XmlNode relshipsNode = secureMessage.SelectSingleNode("Relationships");
				if (relshipsNode != null)
				{
					secureMessage.RemoveChild(relshipsNode);
				}
			}
		}
	}
}
finally
{
	if (permsWasSet)
	{
		Aras.Server.Security.Permissions.RevokeIdentity(identity);
	}
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_SecureMessageOnGet' and [Method].is_current='1'">
<config_id>88387D0D03D545358DA197F2F78C7106</config_id>
<name>VC_SecureMessageOnGet</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
