' name: beforeTreeAdd cvs\Solutions\Solutions\Project\Methods version 1.1
' purpose: APQP. Set project's owner_by_id to all tree items.
' created: 16-APR-2005 Yuri Delyamba
' (c) Copyright by Aras Corporation, 2005-2007.
' MethodTemplateName=VBScriptMainUpgrade;

Sub MethodMainSubroutine()

  Dim project As XmlElement = inDom.DocumentElement
  Dim projectOwner As String = CCO.XML.GetItemProperty(project, "owned_by_id")

  If Not IsNothing(projectOwner) Then

    Dim nodes As XmlNodeList
    Dim node As XmlElement
    Dim i As Integer

    nodes = project.SelectNodes("descendant::Item[@type='WBS Element']")

    For i = 0 To nodes.Count - 1
      node = CType(nodes.Item(i), XmlElement)
      CCO.XML.SetItemProperty(node, "owned_by_id", projectOwner)
    Next

    nodes = project.SelectNodes("descendant::Item[@type='WBS Activity2']/related_id/Item[@type='Activity2']")

    For i = 0 To nodes.Count - 1
      node = CType(nodes.Item(i), XmlElement)
      CCO.XML.SetItemProperty(node, "owned_by_id", projectOwner)
    Next

  End If

End Sub
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='beforeTreeAdd' and [Method].is_current='1'">
<config_id>EF07C047DC7A4BC59AF8E54E31C61A52</config_id>
<name>beforeTreeAdd</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
