Innovator innovator = getInnovator();
Item ruleItem = innovator.newItem("ConversionRule", "get");
ruleItem.setProperty("name", "cmf_ExcelPublishingRule");
ruleItem.setAttribute("levels", "1");
ruleItem = ruleItem.apply();

if (ruleItem.isError())
{
	return ruleItem;
}

string documentTypeId = getProperty("documentTypeId");
string documentId = getProperty("documentId");

Item contentType = innovator.newItem("cmf_ContentType", "get");
contentType.setID(documentTypeId);
contentType = contentType.apply();

var docmentItemTypeId = contentType.getProperty("linked_item_type");

Item resultItem = innovator.applyAML("<AML><Item action=\"get\" typeId=\"" + docmentItemTypeId + "\" id=\"" + documentId + "\" select=\"id\" /></AML>");
if (resultItem.isError())
{
	return resultItem;
}

var documentTypeName = resultItem.getAttribute("type");

var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
string userId = CCO.Variables.GetUserID();
string username = GetUsername(userId);
var taskData = new Dictionary<string, string>
{
	{"documentId", documentId},
	{"documentTypeName", documentTypeName},
	{"username", username}
};

var publishingRule = new Aras.ConversionFramework.Models.ConversionRule() { Item = ruleItem };
var conversionManager = new Aras.ConversionFramework.Management.InnovatorConversionManager(innovator.getConnection());

Aras.Server.Security.Identity conversionManagerIdentity = Aras.Server.Security.Identity.GetById("694C8B27E5D940DAA8BD336E45EC3A63");
bool conversionManagerPermsWasSet = Aras.Server.Security.Permissions.GrantIdentity(conversionManagerIdentity);
string taskId;
try
{
	taskId = conversionManager.CreateConversionTask(publishingRule, task =>
	{
		task.UserData = serializer.Serialize(taskData);
	});
}
finally
{
	if (conversionManagerPermsWasSet)
	{
		Aras.Server.Security.Permissions.RevokeIdentity(conversionManagerIdentity);
	}
}
return innovator.newResult(taskId);
}

private string GetUsername(string userId)
{
	string aml = string.Format(@"      <AML>
	<Item type='User' action='get' select='keyed_name'>
	<id>{0}</id>
	</Item>
	</AML>", userId);
	Item request = newItem();
	request.loadAML(aml);
	Item response = request.apply();
	return response.getProperty("keyed_name");
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_CreateExcelPublishingTask' and [Method].is_current='1'">
<config_id>0EF028BC0CE24BF2A6AA83C3BF725CCE</config_id>
<name>cmf_CreateExcelPublishingTask</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
