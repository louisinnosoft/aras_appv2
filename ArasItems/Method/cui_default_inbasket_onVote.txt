var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onVoteCommand) {
	workerFrame.onVoteCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_inbasket_onVote' and [Method].is_current='1'">
<config_id>D63815EA72874687B9990626F53DE276</config_id>
<name>cui_default_inbasket_onVote</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
