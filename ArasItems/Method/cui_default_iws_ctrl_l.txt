if (window.isEditMode) {
	window.onUnlockCommand();
} else {
	window.onLockCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_iws_ctrl_l' and [Method].is_current='1'">
<config_id>682FA3ED4F9A4B8D83BE762D58453F31</config_id>
<name>cui_default_iws_ctrl_l</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
