var userNd = aras.getLoggedUserItem();
if (!userNd) {
	return false;
}

userNd = userNd.cloneNode(true);
userNd.setAttribute('action', 'get');
userNd.setAttribute('select', 'id');
aras.applyItem(userNd.xml);

var data = {
	title: aras.getResource('', 'common.change_pwd'),
	md5: aras.getPassword(),
	oldMsg: aras.getResource('', 'common.old_pwd'),
	newMsg1: aras.getResource('', 'common.new_pwd'),
	newMsg2: aras.getResource('', 'common.confirm_pwd'),
	errMsg1: aras.getResource('', 'common.old_pwd_wrong'),
	errMsg2: aras.getResource('', 'common.check_pwd_confirmation'),
	errMsg3: aras.getResource('', 'common.pwd_empty'),
	'check_empty': true,
	dialogWidth: 300,
	dialogHeight: 180,
	center: true,
	aras: aras,
	content: 'changeMD5Dialog.html'
};
var methNm = 'User_pwd_checkPolicy';
var methNd = aras.getItem('Method', 'name="' + methNm + '"', '<name>' + methNm + '</name>', 0);
var methCode = methNd ? aras.getItemProperty(methNd, 'method_code') : '';
data['code_to_check_pwd_policy'] = methCode;

var topWindow = aras.getMostTopWindowWithAras(window);
topWindow.ArasModules.Dialog.show('iframe', data).promise.then(function(newPwd) {
	if (newPwd) {
		var r2 = aras.soapSend('ChangeUserPassword', '<new_password>' + newPwd + '</new_password>');
		if (r2.getFaultCode() !== 0) {
			aras.AlertError(r2);
		} else {
			aras.login(aras.getLoginName(), newPwd, aras.getDatabase(), false, aras.getServerBaseURL(), '');
		}
	}
});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_change_passwd' and [Method].is_current='1'">
<config_id>043E63D8EC2C4583AEE8398F0CD58165</config_id>
<name>cui_default_mwmm_change_passwd</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
