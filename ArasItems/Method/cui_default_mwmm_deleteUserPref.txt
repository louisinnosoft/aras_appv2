var topWindow = aras.getMostTopWindowWithAras(window);
var param = {
	title: aras.getResource('', 'deleteprefdlg.title'),
	aras: aras,
	dialogHeight: 400, dialogWidth: 330, resizable: 'yes',
	content: 'SitePreference/deletePreferencesDialog.html'
};
topWindow.ArasModules.Dialog.show('iframe', param);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_deleteUserPref' and [Method].is_current='1'">
<config_id>63D071DA01A2437A980984C10CBDD435</config_id>
<name>cui_default_mwmm_deleteUserPref</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
