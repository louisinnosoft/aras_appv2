var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onMaximizedTabsStateCommand) {
	aras.setPreferenceItemProperties('Core_GlobalLayout', null, {'core_tabs_state': 'maximizedTabsState'});
	menuFrame.onMaximizedTabsStateCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_maxTabsState' and [Method].is_current='1'">
<config_id>08132CE35D914FC5987B595361CCF9E6</config_id>
<name>cui_default_mwmm_maxTabsState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
