var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onOnDStateCommand) {
	localStorage.setItem('defaultDState', 'onDState');
	menuFrame.onOnDStateCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onOnDState' and [Method].is_current='1'">
<config_id>96C81D480D4847C9A4121548C0815640</config_id>
<name>cui_default_mwmm_onOnDState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
