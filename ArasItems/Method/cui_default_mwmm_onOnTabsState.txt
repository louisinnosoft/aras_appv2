var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onOnTabsStateCommand) {
	aras.setPreferenceItemProperties('Core_GlobalLayout', null, {'core_tabs_state': 'tabs on'});
	menuFrame.onOnTabsStateCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onOnTabsState' and [Method].is_current='1'">
<config_id>4846139F51F945C88F2927568A9AF6E6</config_id>
<name>cui_default_mwmm_onOnTabsState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
