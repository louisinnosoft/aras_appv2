var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onNewCommand) {
	workerFrame.onNewCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onNewCommand' and [Method].is_current='1'">
<config_id>B892922796984AD980F33D6664F3012B</config_id>
<name>cui_default_mwt_onNewCommand</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
