' dailyUpdate.vb.txt CVS/Solutions/Project/Methods Revision 1.4

' name: dailyUpdate
' purpose: APQP. Update status and lifecycles.
' created: 29-MAR-2005 Yuri Delyamba
' (c) Copyright by Aras Corporation, 2005-2007.


Dim projects As Item = Me.newItem("Project", "get")
projects.setPropertyAttribute("locked_by_id","condition","is null")
projects = projects.apply()
If projects.isError() Then Return projects

Dim res As New System.Text.StringBuilder("<Item>")

For i As Integer = 0 To projects.getItemCount() - 1
	Dim projectItem As Item = projects.getItemByIndex(i)
	Dim projectState As String = projectItem.getProperty("state")
If String.Equals(projectState,"Active") Then

	projectItem.setAction("Update Project")
	projectItem = projectItem.apply()

	If projectItem.isError() Then
		res.Append(projectItem.getErrorDetail())
	Else
		res.Append(projectItem.getResult())
	End If
	res.Append("<br />")
End If 
Next
res.Append("</Item>")
Me.loadAML(res.ToString())

Return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='dailyUpdate' and [Method].is_current='1'">
<config_id>DBC26D536FD546B4B13FC918E71B0DA4</config_id>
<name>dailyUpdate</name>
<comments>Edit by inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
