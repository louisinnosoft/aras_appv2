//System.Diagnostics.Debugger.Break();
//in_GetUIResources
Innovator inn = this.getInnovator();
string criteria = this.getProperty("criteria");
string UserInfo = this.getProperty("userinfo","");
string r = "";
Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
criteria = "<request><![CDATA[" + criteria + "]]></request>";
try
{
	r =  _InnoApp.BuildResponse("true","ok","in_ui_resources",criteria);
}
catch (Exception ex)
{
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);

}

return  inn.newResult(r);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='in_GetUIResources' and [Method].is_current='1'">
<config_id>1940CCC30D5B47DFA21BBA5BB023B90A</config_id>
<name>in_GetUIResources</name>
<comments>Add By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
