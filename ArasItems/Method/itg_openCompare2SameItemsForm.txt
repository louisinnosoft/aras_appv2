//openCompare2SameItemsForm
var formNd = aras.getItemByName('Form', 'itg_compare2SameItems', 0);

if (formNd) {
	var item = aras.getItemById(this.getType(), this.getID());
	var param = {};
	param.title = 'Item Versions';
	param.formId = formNd.getAttribute('id');
	param.aras = aras;
	param.thisItem = item;
	param.dialogWidth = 800;
	param.dialogHeight = 250;

	var wnd = aras.getMainWindow();
	wnd = wnd.main || wnd;
	param.content = 'ShowFormAsADialog.html';

	wnd.ArasModules.Dialog.show('iframe', param).promise.then(function(result) {
		window.result = result;
	});
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='itg_openCompare2SameItemsForm' and [Method].is_current='1'">
<config_id>1725DB431CDB4CBEBDFDB9BEFE1AFF8A</config_id>
<name>itg_openCompare2SameItemsForm</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
