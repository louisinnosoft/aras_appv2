var topWnd = aras.getMostTopWindowWithAras(window);
if (topWnd.fillDistribution) {
	topWnd.fillDistribution(relationshipID);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='onEMailRowSelect' and [Method].is_current='1'">
<config_id>E48CA3EAB7F044A99D4B9AEB6084746C</config_id>
<name>onEMailRowSelect</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
