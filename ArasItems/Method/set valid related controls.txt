/*version:3*/
function methodExec() {
	var mainDataForm = document.forms.MainDataForm;
	var val = '';
	var relItem = null;
	var relItemExists = false;
	var isDependent;

	var input = mainDataForm.elements['related_id'];
	if (input) {
		val = input.value;
	}

	if (val !== '') {
		relItem = aras.getItemByKeyedName('ItemType', val);

		if (relItem) {
			isDependent = aras.getItemProperty(relItem, 'is_dependent');
			relItemExists = true;
		}
	}

	isDependent = (isDependent == '1');

	var list = {};
	var relatedOptionFld = getFieldByName('related_option');
	var inputs = relatedOptionFld.getElementsByTagName('input');

	for (var i = 0, L = inputs.length; i < L; i++) {
		input = inputs.item(i);
		if (input.type == 'radio') {
			val = input.value;
			if ('0' == val || '1' == val || '2' == val) {
				list[val] = input;
			}
		}
	}

	if (isDependent) {
		if (list['0'].checked) {
			list['2'].checked = true;
		}
		list['0'].disabled = true;
	} else {
		if (list['0'].disabled) {
			list['0'].disabled = false;
		}
	}

	var _rnChb = mainDataForm.elements['related_notnull'];
	if (_rnChb) {
		_rnChb.disabled = !relItemExists;
	}
}

function doWait() {
	if (parent && parent.frames && parent.frames['tearoff_menu'] && parent.frames['tearoff_menu'].setControlEnabled) {
		methodExec();
	} else {
		setTimeout(doWait,10);
	}
}

if (document.isEditMode) {
	doWait();
}
return true;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='set valid related controls' and [Method].is_current='1'">
<config_id>33D6A6BA26AD4B85B35127A53018B565</config_id>
<name>set valid related controls</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
