//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

string aml = "";

aml  = "<AML>";
aml += "<Item type='"+this.getProperty("in_controlled_item_name","")+"WPF' action='add'>";
aml += "<source_id>"+this.getProperty("in_controlled_item_cfgid","")+"</source_id>";
aml += "<related_id>"+this.getID()+"</related_id>";
aml += "</Item></AML>";
Item itmWPF = inn.applyAML(aml);

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='xIn_AddWorkflowProcess' and [Method].is_current='1'">
<config_id>C9D9822F83814339B879371EA2E20FBF</config_id>
<name>xIn_AddWorkflowProcess</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
